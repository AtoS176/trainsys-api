package poweska.app.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import poweska.app.util.finalObject.Edge;
import poweska.app.util.staticMethod.PathReducer;

import java.util.Arrays;
import java.util.List;

public class PathReducerTest {
    @Test
    public void reducePathWithoutChange(){
        //given
        Edge edge1 = new Edge(1L, "Kraków");
        Edge edge2 = new Edge(1L, "Katowice");
        Edge edge3 = new Edge(1L, "Wrocław");
        List<Edge> list = Arrays.asList(edge1, edge2, edge3);
        //when
        List<Edge> reduce = PathReducer.reduce(list);
        //then
        Assert.assertEquals(1, reduce.size());
        Assert.assertEquals(edge3, reduce.get(0));
    }

    @Test
    public void reducePathWithOneChange(){
        //given
        Edge edge1 = new Edge(1L, "Kraków");
        Edge edge2 = new Edge(2L, "Katowice");
        Edge edge3 = new Edge(2L, "Wrocław");
        List<Edge> list = Arrays.asList(edge1, edge2, edge3);
        //when
        List<Edge> reduce = PathReducer.reduce(list);
        //then
        Assert.assertEquals(2, reduce.size());
        Assert.assertEquals(edge1, reduce.get(0));
        Assert.assertEquals(edge3, reduce.get(1));
    }

}
