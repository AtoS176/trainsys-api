package poweska.app.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import poweska.app.domain.Ticket;
import poweska.app.repository.crud.TicketRepository;
import poweska.app.util.staticMethod.PdfGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PdfGeneratorTest {
    @Autowired
    private TicketRepository ticketRepository;

    @Test
    @Transactional
    //Only to show generated file
    public void generateTicket() {
        //given
        Ticket ticket = ticketRepository.findById(1L).get();
        //when
        PdfGenerator.generateTicket(ticket);
        //Then
        Assert.assertTrue(true);
    }
}
