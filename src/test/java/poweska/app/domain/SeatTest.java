package poweska.app.domain;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class SeatTest {
    private final Train train = Mockito.mock(Train.class);

    @BeforeEach
    public void setUp(){
        Mockito.when(train.getLine()).thenReturn(new RailLine(TestData.getFragmentList()));
    }

    @Test
    public void getStartStationContext(){
        //given
        Seat seat = Seat.builder()
                .train(train)
                .from(new Station(3L, "Katowice"))
                .build();
        //when
        String stationContext = seat.getStationContext(Seat.ContextType.FROM);
        //then
        Assert.assertEquals(stationContext, "Katowice(5)");
    }

    @Test
    public void getEndStationContext(){
        //given
        Seat seat = Seat.builder()
                .train(train)
                .to(new Station(4L, "Wrocław"))
                .build();
        //when
        String stationContext = seat.getStationContext(Seat.ContextType.TO);
        //then
        Assert.assertEquals(stationContext, "Wrocław(4)");
    }

}
