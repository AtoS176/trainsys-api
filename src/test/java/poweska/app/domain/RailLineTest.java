package poweska.app.domain;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RailLineTest {
    private final List<LineFragment> fragments = TestData.getFragmentList();

    public RailLine createLine(){
        return new RailLine(fragments);
    }

    @Test
    public void findExistStartPlatform(){
        //given
        RailLine railLine = createLine();
        //when
        int startPlatform = railLine.findStartPlatform(new Station(1L, "Kraków"));
        //then
        Assert.assertEquals(2, startPlatform);
    }

    @Test
    public void findExistEndPlatform(){
        //given
        RailLine railLine = createLine();
        //when
        int startPlatform = railLine.findEndPlatform(new Station(4L, "Wrocław"));
        //then
        Assert.assertEquals(4, startPlatform);
    }

    @Test
    public void findChangeStartPlatform(){
        //given
        RailLine railLine = createLine();
        //when
        int endPlatform = railLine.findEndPlatform(new Station(2L, "Katowice"));
        //then
        Assert.assertEquals(3, endPlatform);
    }

    @Test
    public void findChangeEndPlatform(){
        //given
        RailLine railLine = createLine();
        //when
        int startPlatform = railLine.findStartPlatform(new Station(3L, "Katowice"));
        //then
        Assert.assertEquals(5, startPlatform);
    }

    @Test
    public void findNotExistPlatform(){
        //given
        RailLine railLine = createLine();
        //when

        //then
        assertThrows(NoSuchElementException.class, () -> {
            railLine.findEndPlatform(new Station(5L, "Poznań"));
        });
    }

}
