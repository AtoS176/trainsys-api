package poweska.app.domain;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.util.*;

public interface TestData {
    static List<LineFragment> getFragmentList() {
        StationDetails kr = StationDetails.builder()
                .station(new Station(1L, "Kraków"))
                .platform(2)
                .build();
        StationDetails sk1 = StationDetails.builder()
                .station(new Station(2L, "Katowice"))
                .platform(3)
                .build();
        StationDetails sk2 = StationDetails.builder()
                .station(new Station(3L, "Katowice"))
                .platform(5)
                .build();
        StationDetails dw = StationDetails.builder()
                .station(new Station(4L, "Wrocław"))
                .platform(4)
                .build();
        StationDetails zs = StationDetails.builder()
                .station(new Station(6L, "Szczecin"))
                .platform(4)
                .build();
        LineFragment krSk = LineFragment.builder()
                .start(kr)
                .end(sk1)
                .price(BigDecimal.valueOf(20.0))
                .duration(100)
                .build();
        LineFragment skDw = LineFragment.builder()
                .start(sk2)
                .end(dw)
                .price(BigDecimal.valueOf(35.0))
                .duration(200)
                .build();
        LineFragment dwZs = LineFragment.builder()
                .start(dw)
                .end(zs)
                .price(BigDecimal.valueOf(40.0))
                .duration(220)
                .build();
        return Arrays.asList(krSk ,skDw, dwZs);
    }

    static Set<StartDate> getStartDatesSet(){
        StartDate monday = StartDate.builder()
                .dayOfWeek(DayOfWeek.MONDAY)
                .hour(10)
                .minute(30)
                .build();
        StartDate wednesday = StartDate.builder()
                .dayOfWeek(DayOfWeek.WEDNESDAY)
                .hour(14)
                .minute(0)
                .build();
        StartDate friday = StartDate.builder()
                .dayOfWeek(DayOfWeek.FRIDAY)
                .hour(16)
                .minute(0)
                .build();
        return new LinkedHashSet<>(Arrays.asList(monday, wednesday, friday));
    }

}
