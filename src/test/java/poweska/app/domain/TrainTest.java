package poweska.app.domain;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class TrainTest {
    private RailLine railLine;
    private Map<Integer, List<Seat>> busy;

    @BeforeEach
    public void setData(){
        railLine = new RailLine(TestData.getFragmentList());

        List<Seat> seatsInFirst = spy(List.class);
        List<Seat> seatsInSecond = spy(List.class);
        when(seatsInFirst.size()).thenReturn(10);
        when(seatsInSecond.size()).thenReturn(5);

        busy = new HashMap<>();
        busy.put(1, seatsInFirst);
        busy.put(2, seatsInSecond);
    }

    @Test
    public void calculateTime(){
        //given
        Station dw = new Station(4L, "Wrocław");
        Train train = Train.builder()
                .line(railLine)
                .startDates(TestData.getStartDatesSet())
                .build();
        //when
        LocalDateTime date = train.calculateTime(dw, LocalDate.of(2020, 7, 24));
        //then
        Assert.assertEquals(21, date.getHour());
        Assert.assertEquals(0, date.getMinute());
    }

    @Test
    public void calculateTimeThrowEx(){
        //given
        Station dw = new Station(4L, "Wrocław");
        Train train = Train.builder()
                .line(railLine)
                .startDates(TestData.getStartDatesSet())
                .build();
        //when

        //then
        assertThrows(NoSuchElementException.class, () -> {
            train.calculateTime(dw, LocalDate.of(2020, 7, 25));
        });
    }

    @Test
    public void findAvailableTruck(){
        //given
        Train train = Train.builder()
                .trucksNumber(2)
                .placeInTruck(10)
                .build();
        //when
        int availableTruck = train.findAvailableTruck(busy, 3);
        //then
        Assert.assertEquals(2, availableTruck);
    }

    @Test
    public void findAvailableTruckThrowEx(){
        //given
        Train train = Train.builder()
                .trucksNumber(2)
                .placeInTruck(10)
                .build();
        //when

        //then
        assertThrows(NoSuchElementException.class, () -> {
            train.findAvailableTruck(busy, 6);
        });
    }
}
