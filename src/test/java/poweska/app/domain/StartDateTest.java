package poweska.app.domain;

import org.junit.Assert;

import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public class StartDateTest {
    private final List<StartDate> startDates = new ArrayList<>(TestData.getStartDatesSet());

    @Test
    public void plusMinutes(){
        //given
        StartDate monday = startDates.get(0);
        //when
        StartDate newDate = monday.plusMinutes(100);
        //then
        Assert.assertEquals(newDate.getDayOfWeek(), DayOfWeek.MONDAY);
        Assert.assertEquals(newDate.getHour(), 12);
        Assert.assertEquals(newDate.getMinute(), 10);
    }

    @Test
    public void plusMinutesMidnight(){
        //given
        StartDate wednesday = startDates.get(1);
        //when
        StartDate newDate = wednesday.plusMinutes(600);
        //then
        Assert.assertEquals(newDate.getDayOfWeek(), DayOfWeek.THURSDAY);
        Assert.assertEquals(newDate.getHour(), 0);
        Assert.assertEquals(newDate.getMinute(), 0);
    }

    @Test
    public void plusMinutesNextDay(){
        //given
        StartDate friday = startDates.get(2);
        //when
        StartDate nextDay = friday.plusMinutes(650);
        //then
        Assert.assertEquals(nextDay.getDayOfWeek(), DayOfWeek.SATURDAY);
        Assert.assertEquals(nextDay.getHour(), 2);
        Assert.assertEquals(nextDay.getMinute(), 50);
    }

}

