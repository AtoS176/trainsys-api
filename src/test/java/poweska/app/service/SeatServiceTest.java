package poweska.app.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import poweska.app.domain.SeatType;
import poweska.app.dto.ReservationDTO;
import poweska.app.dto.SeatDTO;
import poweska.app.dto.TicketDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SeatServiceTest {
    @Autowired
    private SeatService seatService;

    @Test
    public void findAvailableTruck(){
        //given
        ReservationDTO reservation = ReservationDTO.builder()
                .when(LocalDate.of(2020, 10, 20))
                .trainsId(Arrays.asList(6L, 3L))
                .stations(Arrays.asList("Poznań", "Łódź", "Gdańsk"))
                .seatTypes(Arrays.asList(SeatType.NORMAL, SeatType.NORMAL))
                .build();
        //when
        List<Integer> trucksNumber = seatService.findAvailableTrucks(reservation);
        //then
        Assert.assertEquals(2, trucksNumber.size());
        Assert.assertEquals(Arrays.asList(1, 1), trucksNumber);
    }

    @Test
    public void findAvailableTruckFirstTruckBusy(){
        //given
        ReservationDTO reservation = ReservationDTO.builder()
                .when(LocalDate.of(2020, 8, 3))
                .trainsId(Collections.singletonList(1L))
                .stations(Arrays.asList("Rzeszów", "Kraków"))
                .seatTypes(Arrays.asList(SeatType.NORMAL, SeatType.NORMAL, SeatType.NORMAL))
                .build();
        //when
        List<Integer> trucksNumber = seatService.findAvailableTrucks(reservation);
        //then
        Assert.assertEquals(1, trucksNumber.size());
        Assert.assertEquals(Collections.singletonList(2), trucksNumber);
    }

    @Test
    public void findSeatsToReserve(){
        //given
        ReservationDTO reservation = ReservationDTO.builder()
                .when(LocalDate.of(2020, 8, 3))
                .trainsId(Collections.singletonList(1L))
                .stations(Arrays.asList("Rzeszów", "Wrocław"))
                .seatTypes(Collections.singletonList(SeatType.STUDENT))
                .email("anna@wp.pl")
                .build();
        //when
        Optional<TicketDTO> ticket = seatService.findSeatsToReserve(reservation);
        //then
        SeatDTO expected = SeatDTO.builder()
                .trainId(1L)
                .trainName("IC305")
                .startTime(LocalDateTime.of(2020, 8, 3, 8, 35))
                .endTime(LocalDateTime.of(2020, 8, 3, 13, 35))
                .truckNumber(2)
                .startStation("Rzeszów")
                .endStation("Wrocław")
                .seatType(SeatType.STUDENT)
                .price(54.00)
                .build();

        Assert.assertTrue(ticket.isPresent());
        Assert.assertEquals(1, ticket.get().getSeatsInTrains().size());
        Assert.assertEquals(expected, ticket.get().getSeatsInTrains().get(0));
    }

}
