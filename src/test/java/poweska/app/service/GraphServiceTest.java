package poweska.app.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import poweska.app.dto.ConnectionDTO;

import java.time.LocalDateTime;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class GraphServiceTest {
    @Autowired
    private GraphService graphService;

    LocalDateTime when = LocalDateTime.of(2020, 7, 24, 0, 0);

    @Test
    public void findConnectionsFromRzToDw(){
        //given
        String from = "Rzeszów";
        String to = "Wrocław";
        //when
        List<ConnectionDTO> connections = graphService.findConnections(from, to, when);
        //then
        Assert.assertEquals(1, connections.size());
        Assert.assertEquals(460, connections.get(0).getDistance());
        Assert.assertEquals(90, connections.get(0).getPrice(), 0.01);
    }

    @Test
    public void findConnectionsFromWiToDw(){
        //given
        String from = "Warszawa";
        String to = "Wrocław";
        //when
        List<ConnectionDTO> connections = graphService.findConnections(from, to, when);
        //then
        Assert.assertEquals(1, connections.size());
        Assert.assertEquals(480, connections.get(0).getDistance());
        Assert.assertEquals(110, connections.get(0).getPrice(), 0.01);
    }

    @Test
    public void findConnectionsFromPoToGd(){
        //given
        String from = "Poznań";
        String to = "Gdańsk";
        //when
        List<ConnectionDTO> connections = graphService.findConnections(from, to, when);
        //then
        Assert.assertEquals(1, connections.size());
        Assert.assertEquals(540, connections.get(0).getDistance());
        Assert.assertEquals(120, connections.get(0).getPrice(), 0.01);
    }

}
