INSERT INTO station(name)
VALUES ('Rzeszów'),
       ('Kraków'),
       ('Katowice'),
       ('Wrocław'),
       ('Szczecin'),
       ('Lublin'),
       ('Warszawa'),
       ('Łódź'),
       ('Poznań'),
       ('Bydgoszcz'),
       ('Białystok'),
       ('Olsztyn'),
       ('Gdańsk');


INSERT INTO station_details(platform, station_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (1, 2),
       (2, 2),
       (3, 2),
       (1, 3),
       (2, 3),
       (3, 3),
       (1, 4),
       (2, 4),
       (3, 4),
       (1, 5),
       (2, 5),
       (3, 5),
       (1, 6),
       (2, 6),
       (3, 6),
       (1, 7),
       (2, 7),
       (3, 7),
       (1, 8),
       (2, 8),
       (3, 8),
       (1, 9),
       (2, 9),
       (3, 9),
       (1, 10),
       (2, 10),
       (3, 10),
       (1, 11),
       (2, 11),
       (3, 11),
       (1, 12),
       (2, 12),
       (3, 12),
       (1, 13),
       (2, 13),
       (3, 13);

-- IC 101, IC102, IC103 : Rzeszów -> Szczecin;

INSERT INTO rail_line(name)
VALUES ('Rzeszów - Szczecin'),
       ('Rzeszów - Szczecin'),
       ('Rzeszów - Szczecin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 20, 4, 1, 1),
       (80, 60, 2, 12, 7, 1, 4),
       (200, 150, 3, 30, 10, 1, 7),
       (240, 180, 4, 30, 13, 1, 10);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC101', 60, 6, 1);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 5, 0),
       ('TUESDAY', 5, 0),
       ('WEDNESDAY', 5, 0),
       ('THURSDAY', 5, 0),
       ('FRIDAY', 5, 0),
       ('SATURDAY', 6, 0),
       ('SUNDAY', 6, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1);


INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 20, 4, 2, 1),
       (80, 60, 2, 12, 7, 2, 4),
       (200, 150, 3, 30, 10, 2, 7),
       (240, 180, 4, 30, 13, 2, 10);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC102', 50, 8, 2);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 11, 0),
       ('TUESDAY', 11, 0),
       ('WEDNESDAY', 11, 0),
       ('THURSDAY', 11, 0),
       ('FRIDAY', 11, 0),
       ('SATURDAY', 12, 0),
       ('SUNDAY', 12, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 2),
       (9, 2),
       (10, 2),
       (11, 2),
       (12, 2),
       (13, 2),
       (14, 2);


INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 20, 4, 3, 1),
       (80, 60, 2, 12, 7, 3, 4),
       (200, 150, 3, 30, 10, 3, 7),
       (240, 180, 4, 30, 13, 3, 10);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC103', 50, 8, 3);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 16, 0),
       ('TUESDAY', 16, 0),
       ('WEDNESDAY', 16, 0),
       ('THURSDAY', 16, 0),
       ('FRIDAY', 16, 0),
       ('SATURDAY', 17, 0),
       ('SUNDAY', 17, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 3),
       (16, 3),
       (17, 3),
       (18, 3),
       (19, 3),
       (20, 3),
       (21, 3);


-- IC104, IC105, IC106 : Szczecin - Rzeszów;

INSERT INTO rail_line(name)
VALUES ('Szczecin - Rzeszów'),
       ('Szczecin - Rzeszów'),
       ('Szczecin - Rzeszów');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 180, 1, 30, 11, 4, 14),
       (200, 150, 2, 30, 9, 4, 11),
       (80, 60, 3, 12, 6, 4, 9),
       (160, 120, 4, 20, 3, 4, 6);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC104', 50, 8, 4);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 4),
       (2, 4),
       (3, 4),
       (4, 4),
       (5, 4),
       (6, 4),
       (7, 4);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 180, 1, 30, 11, 5, 14),
       (200, 150, 2, 30, 9, 5, 11),
       (80, 60, 3, 12, 6, 5, 9),
       (160, 120, 4, 20, 3, 5, 6);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC105', 50, 8, 5);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 5),
       (9, 5),
       (10, 5),
       (11, 5),
       (12, 5),
       (13, 5),
       (14, 5);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 180, 1, 30, 11, 6, 14),
       (200, 150, 2, 30, 9, 6, 11),
       (80, 60, 3, 12, 6, 6, 9),
       (160, 120, 4, 20, 3, 6, 6);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC106', 50, 8, 6);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 6),
       (16, 6),
       (17, 6),
       (18, 6),
       (19, 6),
       (20, 6),
       (21, 6);


-- IC107, IC108 : Katowice - Gdańsk;

INSERT INTO rail_line(name)
VALUES ('Katowice - Gdańsk'),
       ('Katowice - Gdańsk');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 180, 1, 30, 22, 7, 9),
       (220, 180, 2, 30, 28, 7, 22),
       (170, 150, 3, 25, 37, 7, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC107', 50, 9, 7);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 8, 30),
       ('TUESDAY', 8, 30),
       ('WEDNESDAY', 8, 30),
       ('THURSDAY', 8, 30),
       ('FRIDAY', 8, 30),
       ('SATURDAY', 9, 30),
       ('SUNDAY', 9, 30);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 7),
       (23, 7),
       (24, 7),
       (25, 7),
       (26, 7),
       (27, 7),
       (28, 7);


INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 180, 1, 30, 22, 8, 9),
       (220, 180, 2, 30, 28, 8, 22),
       (170, 150, 3, 25, 37, 8, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC108', 50, 9, 8);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 14, 30),
       ('TUESDAY', 14, 30),
       ('WEDNESDAY', 14, 30),
       ('THURSDAY', 14, 30),
       ('FRIDAY', 14, 30),
       ('SATURDAY', 15, 30),
       ('SUNDAY', 15, 30);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 8),
       (30, 8),
       (31, 8),
       (32, 8),
       (33, 8),
       (34, 8),
       (35, 8);

-- IC109, IC110 : Gdańsk - Katowice;

INSERT INTO rail_line(name)
VALUES ('Gdańsk - Katowice'),
       ('Gdańsk - Katowice');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 150, 1, 25, 30, 9, 39),
       (220, 180, 2, 30, 23, 9, 30),
       (200, 180, 3, 30, 8, 9, 23);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC109', 50, 8, 9);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 9),
       (2, 9),
       (3, 9),
       (4, 9),
       (5, 9),
       (6, 9),
       (7, 9);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 150, 1, 25, 30, 10, 39),
       (220, 180, 2, 30, 23, 10, 30),
       (200, 180, 3, 30, 8, 10, 23);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC110', 60, 7, 10);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 10),
       (9, 10),
       (10, 10),
       (11, 10),
       (12, 10),
       (13, 10),
       (14, 10);

-- IC111, IC112 : Lublin - Szczecin;

INSERT INTO rail_line(name)
VALUES ('Lublin - Szczecin'),
       ('Lublin - Szczecin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (150, 120, 1, 20, 19, 11, 17),
       (100, 60, 2, 15, 22, 11, 19),
       (200, 150, 3, 25, 26, 11, 22),
       (240, 160, 4, 30, 15, 11, 26);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC111', 60, 6, 11);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 7, 0),
       ('TUESDAY', 7, 0),
       ('WEDNESDAY', 7, 0),
       ('THURSDAY', 7, 0),
       ('FRIDAY', 7, 0),
       ('SATURDAY', 8, 0),
       ('SUNDAY', 8, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 11),
       (37, 11),
       (38, 11),
       (39, 11),
       (40, 11),
       (41, 11),
       (42, 11);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (150, 120, 1, 20, 19, 12, 17),
       (100, 60, 2, 15, 22, 12, 19),
       (200, 150, 3, 25, 26, 12, 22),
       (240, 160, 4, 30, 15, 12, 26);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC112', 60, 6, 12);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 12),
       (16, 12),
       (17, 12),
       (18, 12),
       (19, 12),
       (20, 12),
       (21, 12);

-- IC113, 114 : Szczecin - Lublin;

INSERT INTO rail_line(name)
VALUES ('Szczecin - Lublin'),
       ('Szczecin - Lublin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 160, 1, 30, 25, 13, 15),
       (200, 150, 2, 25, 23, 13, 25),
       (100, 60, 3, 15, 20, 13, 23),
       (150, 120, 4, 20, 17, 13, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC113', 60, 7, 13);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 13),
       (2, 13),
       (3, 13),
       (4, 13),
       (5, 13),
       (6, 13),
       (7, 13);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 160, 1, 30, 25, 14, 15),
       (200, 150, 2, 25, 23, 14, 25),
       (100, 60, 3, 15, 20, 14, 23),
       (150, 120, 4, 20, 17, 14, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC114', 60, 7, 14);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 14),
       (9, 14),
       (10, 14),
       (11, 14),
       (12, 14),
       (13, 14),
       (14, 14);

-- IC115 : Rzeszów - Białystok;

INSERT INTO rail_line(name)
VALUES ('Rzeszów - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 120, 1, 30, 18, 15, 3),
       (250, 180, 2, 35, 32, 15, 18);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC115', 60, 7, 15);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 13, 45),
       ('TUESDAY', 13, 45),
       ('WEDNESDAY', 13, 45),
       ('THURSDAY', 13, 45),
       ('FRIDAY', 13, 45),
       ('SATURDAY', 14, 45),
       ('SUNDAY', 14, 45);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (43, 15),
       (44, 15),
       (45, 15),
       (46, 15),
       (47, 15),
       (48, 15),
       (49, 15);

-- IC116 : Białystok - Rzeszów;

INSERT INTO rail_line(name)
VALUES ('Białystok - Rzeszów');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (250, 180, 1, 35, 18, 16, 32),
       (170, 120, 2, 30, 2, 16, 18);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC116', 50, 6, 16);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 16),
       (2, 16),
       (3, 16),
       (4, 16),
       (5, 16),
       (6, 16),
       (7, 16);


-- IC117, IC118 : Białystok - Gdańsk;

INSERT INTO rail_line(name)
VALUES ('Białystok - Gdańsk'),
       ('Białystok - Gdańsk');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (230, 180, 1, 30, 34, 17, 31),
       (160, 120, 2, 25, 38, 17, 34);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC117', 50, 7, 17);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 19, 15),
       ('TUESDAY', 19, 15),
       ('WEDNESDAY', 19, 15),
       ('THURSDAY', 19, 15),
       ('FRIDAY', 19, 15),
       ('SATURDAY', 20, 15),
       ('SUNDAY', 20, 15);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 17),
       (51, 17),
       (52, 17),
       (53, 17),
       (54, 17),
       (55, 17),
       (56, 17);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (230, 180, 1, 30, 34, 18, 31),
       (160, 120, 2, 25, 38, 18, 34);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC118', 50, 6, 18);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 18),
       (9, 18),
       (10, 18),
       (11, 18),
       (12, 18),
       (13, 18),
       (14, 18);

-- IC119, IC120 : Gdańsk - Białystok;

INSERT INTO rail_line(name)
VALUES ('Gdańsk - Białystok'),
       ('Gdańsk - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 25, 35, 19, 39),
       (230, 180, 2, 30, 33, 19, 35);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC119', 50, 6, 19);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 19),
       (9, 19),
       (10, 19),
       (11, 19),
       (12, 19),
       (13, 19),
       (14, 19);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 25, 35, 20, 39),
       (230, 180, 2, 30, 33, 20, 35);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC120', 50, 6, 20);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 18, 15),
       ('TUESDAY', 18, 15),
       ('WEDNESDAY', 18, 15),
       ('THURSDAY', 18, 15),
       ('FRIDAY', 18, 15),
       ('SATURDAY', 19, 15),
       ('SUNDAY', 19, 15);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 20),
       (58, 20),
       (59, 20),
       (60, 20),
       (61, 20),
       (62, 20),
       (63, 20);

-- IC121 : Warszawa - Szczecin;

INSERT INTO rail_line(name)
VALUES ('Warszawa - Szczecin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 40, 30, 21, 21),
       (260, 150, 2, 30, 15, 21, 30);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 10, 10),
       ('TUESDAY', 10, 10),
       ('WEDNESDAY', 10, 10),
       ('THURSDAY', 10, 10),
       ('FRIDAY', 10, 10),
       ('SATURDAY', 11, 10),
       ('SUNDAY', 11, 10);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC121', 50, 6, 21);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (64, 21),
       (65, 21),
       (66, 21),
       (67, 21),
       (68, 21),
       (69, 21),
       (70, 21);

-- IC122 : Szczecin - Warszawa;

INSERT INTO rail_line(name)
VALUES ('Szczecin - Warszawa');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (260, 150, 1, 30, 29, 22, 15),
       (300, 180, 2, 40, 21, 22, 29);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC122', 60, 6, 22);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 22),
       (9, 22),
       (10, 22),
       (11, 22),
       (12, 22),
       (13, 22),
       (14, 22);


-- IC123, 124 : Białystok : Kraków;

INSERT INTO rail_line(name)
VALUES ('Białystok - Kraków');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 200, 1, 35, 21, 23, 31),
       (300, 220, 2, 40, 5, 23, 21);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC123', 50, 8, 23);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 23),
       (37, 23),
       (38, 23),
       (39, 23),
       (40, 23),
       (41, 23),
       (42, 23);

INSERT INTO rail_line(name)
VALUES ('Białystok - Kraków');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 200, 1, 35, 21, 24, 31),
       (300, 220, 2, 40, 5, 24, 21);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC124', 50, 8, 24);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (43, 24),
       (44, 24),
       (45, 24),
       (46, 24),
       (47, 24),
       (48, 24),
       (49, 24);


-- IC125, 126 : Kraków - Białystok;

INSERT INTO rail_line(name)
VALUES ('Kraków - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 220, 1, 40, 20, 25, 4),
       (220, 200, 2, 35, 32, 25, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC125', 50, 8, 25);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 25),
       (23, 25),
       (24, 25),
       (25, 25),
       (26, 25),
       (27, 25),
       (28, 25);

INSERT INTO rail_line(name)
VALUES ('Kraków - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 220, 1, 40, 20, 26, 5),
       (220, 200, 2, 35, 32, 26, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC126', 40, 9, 26);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 12, 0),
       ('TUESDAY', 12, 0),
       ('WEDNESDAY', 12, 0),
       ('THURSDAY', 12, 0),
       ('FRIDAY', 12, 0),
       ('SATURDAY', 13, 0),
       ('SUNDAY', 13, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (71, 26),
       (72, 26),
       (73, 26),
       (74, 26),
       (75, 26),
       (76, 26),
       (77, 26);

-- IC127, 128 : Wrocław - Olsztyn;

INSERT INTO rail_line(name)
VALUES ('Wrocław - Olsztyn'),
       ('Wrocław - Olsztyn');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 120, 1, 25, 26, 27, 10),
       (200, 150, 2, 25, 23, 27, 26),
       (100, 60, 3, 15, 21, 27, 23),
       (220, 180, 4, 30, 36, 27, 21);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC127', 50, 6, 27);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 27),
       (2, 27),
       (3, 27),
       (4, 27),
       (5, 27),
       (6, 27),
       (7, 27);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 120, 1, 25, 26, 28, 10),
       (200, 150, 2, 25, 23, 28, 26),
       (100, 60, 3, 15, 21, 28, 23),
       (220, 180, 4, 30, 36, 28, 21);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC128', 60, 6, 28);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (43, 28),
       (44, 28),
       (45, 28),
       (46, 28),
       (47, 28),
       (48, 28),
       (49, 28);

-- IC129, 130 : Olsztyn - Wrocław;

INSERT INTO rail_line(name)
VALUES ('Olsztyn - Wrocław'),
       ('Olsztyn - Wrocław');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 180, 1, 30, 20, 29, 34),
       (100, 60, 2, 15, 23, 29, 20),
       (200, 150, 3, 25, 25, 29, 23),
       (180, 120, 4, 25, 10, 29, 25);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC129', 60, 6, 29);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 29),
       (30, 29),
       (31, 29),
       (32, 29),
       (33, 29),
       (34, 29),
       (35, 29);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 180, 1, 30, 20, 30, 34),
       (100, 60, 2, 15, 23, 30, 20),
       (200, 150, 3, 25, 25, 30, 23),
       (180, 120, 4, 25, 10, 30, 25);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC130', 50, 6, 30);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 30),
       (23, 30),
       (24, 30),
       (25, 30),
       (26, 30),
       (27, 30),
       (28, 30);

-- IC131, 132 : Katowice - Białystok;

INSERT INTO rail_line(name)
VALUES ('Katowice - Białystok'),
       ('Katowice - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (80, 60, 1, 12, 5, 31, 8),
       (300, 150, 2, 40, 19, 31, 5),
       (220, 150, 3, 30, 33, 31, 19);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC131', 50, 7, 31);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 31),
       (16, 31),
       (17, 31),
       (18, 31),
       (19, 31),
       (20, 31),
       (21, 31);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (80, 60, 1, 12, 5, 32, 8),
       (300, 150, 2, 40, 19, 32, 5),
       (220, 150, 3, 30, 33, 32, 19);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC132', 50, 7, 32);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 32),
       (51, 32),
       (52, 32),
       (53, 32),
       (54, 32),
       (55, 32),
       (56, 32);

-- IC133, 134 : Białystok - Katowice;

INSERT INTO rail_line(name)
VALUES ('Białystok - Katowice'),
       ('Białystok - Katowice');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 150, 1, 30, 19, 33, 33),
       (300, 150, 2, 40, 5, 33, 19),
       (80, 60, 3, 12, 9, 33, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC133', 40, 8, 33);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 33),
       (51, 33),
       (52, 33),
       (53, 33),
       (54, 33),
       (55, 33),
       (56, 33);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (220, 150, 1, 30, 19, 34, 33),
       (300, 150, 2, 40, 5, 34, 19),
       (80, 60, 3, 12, 9, 34, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC134', 40, 8, 34);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 34),
       (16, 34),
       (17, 34),
       (18, 34),
       (19, 34),
       (20, 34),
       (21, 34);

-- IC135, IC136 : Gdańsk - Lublin;

INSERT INTO rail_line(name)
VALUES ('Gdańsk - Lublin'),
       ('Gdańsk - Lublin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 30, 19, 35, 37),
       (180, 120, 2, 20, 18, 35, 19);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC135', 50, 8, 35);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 35),
       (37, 35),
       (38, 35),
       (39, 35),
       (40, 35),
       (41, 35),
       (42, 35);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 30, 19, 36, 37),
       (180, 120, 2, 20, 18, 36, 19);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC136', 50, 7, 36);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 36),
       (58, 36),
       (59, 36),
       (60, 36),
       (61, 36),
       (62, 36),
       (63, 36);

-- IC137, IC138 : Lublin - Gdańsk;

INSERT INTO rail_line(name)
VALUES ('Lublin - Gdańsk'),
       ('Lublin - Gdańsk');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 120, 1, 20, 20, 37, 16),
       (300, 180, 2, 30, 37, 37, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC137', 50, 8, 37);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 37),
       (2, 37),
       (3, 37),
       (4, 37),
       (5, 37),
       (6, 37),
       (7, 37);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 120, 1, 20, 20, 38, 16),
       (300, 180, 2, 30, 37, 38, 20);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC138', 40, 8, 38);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 38),
       (51, 38),
       (52, 38),
       (53, 38),
       (54, 38),
       (55, 38),
       (56, 38);

-- IC139 : Rzeszów - Gdańsk;

INSERT INTO rail_line(name)
VALUES ('Rzeszów - Gdańsk');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 15, 6, 39, 2),
       (290, 180, 2, 30, 23, 39, 6),
       (210, 170, 3, 25, 29, 39, 23),
       (170, 140, 4, 25, 37, 39, 29);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC139', 50, 8, 39);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 39),
       (37, 39),
       (38, 39),
       (39, 39),
       (40, 39),
       (41, 39),
       (42, 39);

-- IC140 : Gdańsk - Rzeszów;

INSERT INTO rail_line(name)
VALUES ('Gdańsk - Rzeszów');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 140, 1, 25, 28, 40, 38),
       (210, 170, 2, 25, 24, 40, 28),
       (290, 180, 3, 30, 4, 40, 24),
       (160, 120, 4, 15, 3, 40, 4);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC140', 50, 8, 40);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 40),
       (51, 40),
       (52, 40),
       (53, 40),
       (54, 40),
       (55, 40),
       (56, 40);

-- IC141, 142 : Białystok - Szczecin;

INSERT INTO rail_line(name)
VALUES ('Białystok - Szczecin'),
       ('Białystok - Szczecin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (230, 180, 1, 25, 34, 41, 31),
       (160, 120, 2, 20, 37, 41, 34),
       (160, 120, 3, 20, 28, 41, 37),
       (260, 180, 4, 30, 13, 41, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC141', 50, 8, 41);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 41),
       (23, 41),
       (24, 41),
       (25, 41),
       (26, 41),
       (27, 41),
       (28, 41);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (230, 180, 1, 25, 34, 42, 31),
       (160, 120, 2, 20, 37, 42, 34),
       (160, 120, 3, 20, 28, 42, 37),
       (260, 180, 4, 30, 13, 42, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC142', 50, 8, 42);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 42),
       (30, 42),
       (31, 42),
       (32, 42),
       (33, 42),
       (34, 42),
       (35, 42);

-- IC143, 144 : Szczecin - Białystok;

INSERT INTO rail_line(name)
VALUES ('Szczecin - Białystok'),
       ('Szczecin - Białystok');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (260, 180, 1, 30, 29, 43, 13),
       (160, 120, 2, 20, 37, 43, 29),
       (160, 120, 3, 20, 35, 43, 37),
       (230, 180, 4, 25, 33, 43, 35);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC143', 50, 8, 43);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 43),
       (37, 43),
       (38, 43),
       (39, 43),
       (40, 43),
       (41, 43),
       (42, 43);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (260, 180, 1, 30, 29, 44, 13),
       (160, 120, 2, 20, 37, 44, 29),
       (160, 120, 3, 20, 35, 44, 37),
       (230, 180, 4, 25, 33, 44, 35);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC144', 60, 6, 44);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 44),
       (58, 44),
       (59, 44),
       (60, 44),
       (61, 44),
       (62, 44),
       (63, 44);

INSERT INTO station (name)
VALUES ('Zakopane'),
       ('Koszalin');

INSERT INTO station_details(platform, station_id)
VALUES (1, 14),
       (2, 14),
       (3, 14),
       (1, 15),
       (2, 15),
       (3, 15);

-- IC145, 146 : Zakopane - Gdańsk;

INSERT INTO rail_line(name)
VALUES ('Zakopane - Gdańsk'),
       ('Zakopane - Gdańsk');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 12, 5, 45, 40),
       (80, 60, 2, 12, 7, 45, 5),
       (240, 150, 3, 30, 23, 45, 7),
       (170, 110, 4, 25, 28, 45, 23),
       (160, 120, 5, 25, 39, 45, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC145', 60, 6, 45);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 45),
       (2, 45),
       (3, 45),
       (4, 45),
       (5, 45),
       (6, 45),
       (7, 45);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 12, 5, 46, 40),
       (80, 60, 2, 12, 7, 46, 5),
       (240, 150, 3, 30, 23, 46, 7),
       (170, 110, 4, 25, 28, 46, 23),
       (160, 120, 5, 25, 39, 46, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC146', 60, 6, 46);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 46),
       (16, 46),
       (17, 46),
       (18, 46),
       (19, 46),
       (20, 46),
       (21, 46);

-- IC147, 148 : Gdańsk - Zakopane;

INSERT INTO rail_line(name)
VALUES ('Gdańsk - Zakopane'),
       ('Gdańsk - Zakopane');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 25, 30, 47, 38),
       (170, 110, 2, 25, 22, 47, 30),
       (240, 150, 3, 30, 9, 47, 22),
       (80, 60, 4, 12, 5, 47, 9),
       (100, 60, 5, 12, 42, 47, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC147', 60, 6, 47);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (36, 47),
       (37, 47),
       (38, 47),
       (39, 47),
       (40, 47),
       (41, 47),
       (42, 47);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 25, 30, 48, 38),
       (170, 110, 2, 25, 22, 48, 30),
       (240, 150, 3, 30, 9, 48, 22),
       (80, 60, 4, 12, 5, 48, 9),
       (100, 60, 5, 12, 42, 48, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC148', 60, 6, 48);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 48),
       (51, 48),
       (52, 48),
       (53, 48),
       (54, 48),
       (55, 48),
       (56, 48);

-- IC149, 150 : Zakopane - Poznań;

INSERT INTO rail_line(name)
VALUES ('Zakopane - Poznań'),
       ('Zakopane - Poznań');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 12, 4, 49, 41),
       (80, 60, 2, 12, 7, 49, 4),
       (220, 120, 3, 25, 11, 49, 7),
       (180, 160, 4, 25, 27, 49, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC149', 60, 6, 49);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 49),
       (23, 49),
       (24, 49),
       (25, 49),
       (26, 49),
       (27, 49),
       (28, 49);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 12, 4, 50, 41),
       (80, 60, 2, 12, 7, 50, 4),
       (220, 120, 3, 25, 11, 50, 7),
       (180, 160, 4, 25, 27, 50, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC150', 50, 8, 50);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 50),
       (30, 50),
       (31, 50),
       (32, 50),
       (33, 50),
       (34, 50),
       (35, 50);

-- IC151, 152 : Poznań - Zakopane;

INSERT INTO rail_line(name)
VALUES ('Poznań - Zakopane'),
       ('Poznań - Zakopane');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 160, 1, 25, 11, 51, 27),
       (220, 120, 2, 25, 8, 51, 11),
       (80, 60, 3, 12, 6, 51, 8),
       (100, 60, 4, 15, 42, 51, 6);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC151', 50, 8, 51);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (64, 51),
       (65, 51),
       (66, 51),
       (67, 51),
       (68, 51),
       (69, 51),
       (70, 51);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 160, 1, 25, 11, 52, 27),
       (220, 120, 2, 25, 8, 52, 11),
       (80, 60, 3, 12, 6, 52, 8),
       (100, 60, 4, 15, 42, 52, 6);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC152', 40, 8, 52);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 52),
       (58, 52),
       (59, 52),
       (60, 52),
       (61, 52),
       (62, 52),
       (63, 52);

-- IC153, 154, 155 : Zakopane - Warszawa;

INSERT INTO rail_line(name)
VALUES ('Zakopane - Warszawa'),
       ('Zakopane - Warszawa'),
       ('Zakopane - Warszawa');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 15, 5, 53, 42),
       (300, 180, 2, 35, 20, 53, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC153', 53, 8, 53);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (64, 53),
       (65, 53),
       (66, 53),
       (67, 53),
       (68, 53),
       (69, 53),
       (70, 53);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 15, 5, 54, 42),
       (300, 180, 2, 35, 20, 54, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC154', 40, 7, 54);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (43, 54),
       (44, 54),
       (45, 54),
       (46, 54),
       (47, 54),
       (48, 54),
       (49, 54);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (100, 60, 1, 15, 5, 55, 42),
       (300, 180, 2, 35, 20, 55, 5);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC155', 40, 8, 55);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 55),
       (58, 55),
       (59, 55),
       (60, 55),
       (61, 55),
       (62, 55),
       (63, 55);

-- IC156, 157, 158 : Warszawa - Zakopane;

INSERT INTO rail_line(name)
VALUES ('Warszawa - Zakopane'),
       ('Warszawa - Zakopane'),
       ('Warszawa - Zakopane');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 35, 4, 56, 19),
       (100, 60, 2, 15, 42, 56, 4);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC156', 50, 8, 56);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 56),
       (2, 56),
       (3, 56),
       (4, 56),
       (5, 56),
       (6, 56),
       (7, 56);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 35, 4, 57, 19),
       (100, 60, 2, 15, 42, 57, 4);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC157', 50, 8, 57);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (71, 57),
       (72, 57),
       (73, 57),
       (74, 57),
       (75, 57),
       (76, 57),
       (77, 57);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (300, 180, 1, 35, 4, 58, 20),
       (100, 60, 2, 15, 42, 58, 4);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC158', 40, 9, 58);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 58),
       (51, 58),
       (52, 58),
       (53, 58),
       (54, 58),
       (55, 58),
       (56, 58);

-- IC159, 160 : Koszalin - Katowice;

INSERT INTO rail_line(name)
VALUES ('Koszalin - Katowice'),
       ('Koszalin - Katowice');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 160, 1, 25, 28, 59, 43),
       (180, 120, 2, 25, 22, 59, 28),
       (240, 180, 3, 30, 8, 59, 22);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC159', 50, 8, 59);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 59),
       (23, 59),
       (24, 59),
       (25, 59),
       (26, 59),
       (27, 59),
       (28, 59);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 160, 1, 25, 28, 60, 43),
       (180, 120, 2, 25, 22, 60, 28),
       (240, 180, 3, 30, 8, 60, 22);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC160', 40, 8, 60);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (43, 60),
       (44, 60),
       (45, 60),
       (46, 60),
       (47, 60),
       (48, 60),
       (49, 60);

-- IC161, 162 : Katowice - Koszalin;

INSERT INTO rail_line(name)
VALUES ('Katowice - Koszalin'),
       ('Katowice - Koszalin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 180, 1, 30, 23, 61, 8),
       (180, 120, 2, 25, 28, 61, 23),
       (200, 160, 3, 25, 45, 61, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC161', 40, 8, 61);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (64, 61),
       (65, 61),
       (66, 61),
       (67, 61),
       (68, 61),
       (69, 61),
       (70, 61);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (240, 180, 1, 30, 23, 62, 8),
       (180, 120, 2, 25, 28, 62, 23),
       (200, 160, 3, 25, 45, 62, 28);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC162', 40, 8, 62);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 62),
       (58, 62),
       (59, 62),
       (60, 62),
       (61, 62),
       (62, 62),
       (63, 62);

-- IC163 : Koszalin - Rzeszów;

INSERT INTO rail_line(name)
VALUES ('Koszalin - Rzeszów');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 160, 1, 25, 28, 63, 43),
       (260, 180, 2, 30, 21, 63, 29),
       (170, 120, 3, 20, 18, 63, 21),
       (180, 150, 4, 25, 1, 63, 18);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC163', 50, 8, 63);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 63),
       (30, 63),
       (31, 63),
       (32, 63),
       (33, 63),
       (34, 63),
       (35, 63);


-- IC164 : Rzeszów - Koszalin;

INSERT INTO rail_line(name)
VALUES ('Rzeszów - Koszalin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 150, 1, 25, 18, 64, 3),
       (170, 120, 2, 20, 19, 64, 18),
       (260, 180, 3, 30, 29, 64, 19),
       (200, 160, 4, 25, 44, 64, 29);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC164', 50, 8, 64);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 64),
       (2, 64),
       (3, 64),
       (4, 64),
       (5, 64),
       (6, 64),
       (7, 64);

-- IC165, IC166 : Koszalin - Wrocław;

INSERT INTO rail_line(name)
VALUES ('Koszalin - Wrocław'),
       ('Koszalin - Wrocław');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (250, 180, 1, 30, 26, 65, 44),
       (180, 150, 2, 25, 11, 65, 26);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC165', 50, 8, 65);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (29, 65),
       (30, 65),
       (31, 65),
       (32, 65),
       (33, 65),
       (34, 65),
       (35, 65);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (250, 180, 1, 30, 26, 66, 44),
       (180, 150, 2, 25, 11, 66, 26);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC166', 50, 8, 66);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 66),
       (51, 66),
       (52, 66),
       (53, 66),
       (54, 66),
       (55, 66),
       (56, 66);

-- IC167, IC168 : Wrocław - Koszalin;

INSERT INTO rail_line(name)
VALUES ('Wrocław - Koszalin'),
       ('Wrocław - Koszalin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 150, 1, 25, 25, 67, 12),
       (250, 180, 2, 30, 45, 67, 25);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC167', 40, 8, 67);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (8, 67),
       (9, 67),
       (10, 67),
       (11, 67),
       (12, 67),
       (13, 67),
       (14, 67);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (180, 150, 1, 25, 25, 68, 12),
       (250, 180, 2, 30, 45, 68, 25);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC168', 50, 8, 68);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (15, 68),
       (16, 68),
       (17, 68),
       (18, 68),
       (19, 68),
       (20, 68),
       (21, 68);

-- IC169, IC170 : Szczecin - Olsztyn;

INSERT INTO rail_line(name)
VALUES ('Szczecin - Olsztyn'),
       ('Szczecin - Olsztyn');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 130, 1, 25, 45, 69, 13),
       (190, 150, 2, 30, 38, 69, 45),
       (170, 140, 3, 25, 35, 69, 38);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC169', 40, 6, 69);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (22, 69),
       (23, 69),
       (24, 69),
       (25, 69),
       (26, 69),
       (27, 69),
       (28, 69);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 130, 1, 25, 45, 70, 13),
       (190, 150, 2, 30, 38, 70, 45),
       (170, 140, 3, 25, 35, 70, 38);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC170', 40, 6, 70);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (57, 70),
       (58, 70),
       (59, 70),
       (60, 70),
       (61, 70),
       (62, 70),
       (63, 70);

-- IC171, IC172 : Olsztyn - Szczecin;

INSERT INTO rail_line(name)
VALUES ('Olsztyn - Szczecin'),
       ('Olsztyn - Szczecin');

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 140, 1, 25, 38, 71, 36),
       (190, 150, 2, 30, 44, 71, 38),
       (170, 130, 3, 25, 14, 71, 44);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC171', 40, 6, 71);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 71),
       (2, 71),
       (3, 71),
       (4, 71),
       (5, 71),
       (6, 71),
       (7, 71);

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (170, 140, 1, 25, 38, 72, 36),
       (190, 150, 2, 30, 44, 72, 38),
       (170, 130, 3, 25, 14, 72, 44);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC172', 40, 7, 72);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (50, 72),
       (51, 72),
       (52, 72),
       (53, 72),
       (54, 72),
       (55, 72),
       (56, 72);