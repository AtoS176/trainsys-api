INSERT INTO station(name)
VALUES ('Rzeszów'),
       ('Kraków'),
       ('Wrocław'),
       ('Poznań'),
       ('Warszawa'),
       ('Łódź'),
       ('Gdańsk');

INSERT INTO station_details(platform, station_id)
VALUES (1, 1),
       (2, 1),
       (1, 2),
       (2, 2),
       (1, 3),
       (2, 3),
       (1, 4),
       (2, 4),
       (1, 5),
       (2, 5),
       (1, 6),
       (2, 6),
       (1, 7),
       (2, 7);

INSERT INTO rail_line(name)
VALUES ('Rzeszów - Poznań'),
       ('Poznań - Rzeszów'),
       ('Kraków - Gdańsk'),
       ('Gdańsk - Kraków'),
       ('Warszawa - Poznań'),
       ('Poznań - Warszawa');

# IC305, Rzeszów -> Kraków -> Wrocław -> Poznań;

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 30, 3, 1, 1),
       (300, 180, 2, 60, 5, 1, 3),
       (160, 120, 3, 30, 8, 1, 5);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 8, 35),
       ('TUESDAY', 8, 35),
       ('WEDNESDAY', 8, 35),
       ('THURSDAY', 10, 50),
       ('FRIDAY', 10, 50);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC305', 3, 8, 1);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1);

#IC350, Poznań -> Wrocław -> Kraków -> Rzeszów;

INSERT INTO line_fragment (distance, duration, number, price, end_id, line_id, start_id)
VALUES (160, 120, 1, 30, 5, 2, 7),
       (300, 180, 2, 60, 4, 2, 5),
       (160, 120, 3, 30, 2, 2, 4);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC350', 3, 8, 2);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 7, 55),
       ('TUESDAY', 7, 55),
       ('WEDNESDAY', 7, 55),
       ('THURSDAY', 13, 0),
       ('FRIDAY', 16, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (6, 2),
       (7, 2),
       (8, 2),
       (9, 2),
       (10, 2);

#IC408, Kraków -> Łódź -> Gdańsk;

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (280, 210, 1, 50, 11, 3, 3),
       (340, 260, 2, 70, 13, 3, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC408', 3, 6, 3);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 8, 0),
       ('TUESDAY', 8, 0),
       ('WEDNESDAY', 10, 0),
       ('THURSDAY', 10, 0),
       ('FRIDAY', 10, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (11, 3),
       (12, 3),
       (13, 3),
       (14, 3),
       (15, 3);

#IC480, Gdańsk -> Łodz -> Kraków;

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (340, 260, 1, 70, 11, 4, 14),
       (280, 210, 2, 50, 4, 4, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC480', 3, 6, 4);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 7, 0),
       ('TUESDAY', 7, 0),
       ('WEDNESDAY', 7, 0),
       ('THURSDAY', 11, 0),
       ('FRIDAY', 11, 0);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (16, 4),
       (17, 4),
       (18, 4),
       (19, 4),
       (20, 4);

# IC505, Warszawa -> Łódz -> Poznań;

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (120, 100, 1, 30, 11, 5, 9),
       (200, 200, 2, 50, 7, 5, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC505', 3, 6, 5);

INSERT INTO start_date(day_of_week, hour, minute)
VALUES ('MONDAY', 6, 0),
       ('TUESDAY', 6, 0),
       ('WEDNESDAY', 9, 0),
       ('THURSDAY', 10, 30),
       ('FRIDAY', 10, 30);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (21, 5),
       (22, 5),
       (23, 5),
       (24, 5),
       (25, 5);

# IC550, Poznań -> Łódz -> Warszawa;

INSERT INTO line_fragment(distance, duration, number, price, end_id, line_id, start_id)
VALUES (200, 200, 1, 50, 11, 6, 7),
       (120, 100, 2, 30, 10, 6, 11);

INSERT INTO train(name, place_in_truck, trucks_number, line_id)
VALUES ('IC550', 3, 6, 6);

INSERT INTO train_start_date(start_date_id, train_id)
VALUES (11, 6),
       (12, 6),
       (13, 6),
       (14, 6),
       (15, 6);

# TEST SIMPLE TICKET;

INSERT INTO buyer(email)
VALUES ('asia@wp.pl');

INSERT INTO ticket(id, price, buyer_id, code)
VALUES (1, 234, 1, 'Jww341');

INSERT INTO seat(date, price, seat_type, truck_number, from_id, ticket_id, to_id, train_id)
VALUES ('2020-08-03', 90, 'NORMAL', 1, 1, 1, 3, 1),
       ('2020-08-03', 90, 'NORMAL', 1, 1, 1, 3, 1),
       ('2020-08-03', 54, 'STUDENT', 1, 1, 1, 3, 1);