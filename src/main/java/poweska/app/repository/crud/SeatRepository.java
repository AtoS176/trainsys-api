package poweska.app.repository.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import poweska.app.domain.Seat;

import java.time.LocalDate;
import java.util.Set;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {
    Set<Seat> findByTrainIdAndDate(Long trainId, LocalDate when);
}
