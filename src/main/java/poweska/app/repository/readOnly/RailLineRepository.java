package poweska.app.repository.readOnly;

import org.springframework.stereotype.Repository;
import poweska.app.domain.RailLine;

@Repository
public interface RailLineRepository extends ReadOnlyRepository<RailLine, Long> {
}
