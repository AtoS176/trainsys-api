package poweska.app.repository.readOnly;

import org.springframework.stereotype.Repository;
import poweska.app.domain.Station;

@Repository
public interface StationRepository extends ReadOnlyRepository<Station, Long> {
    Station findByName(String name);
}
