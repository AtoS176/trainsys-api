package poweska.app.repository.readOnly;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import poweska.app.domain.Train;

import java.util.Set;

@Repository
public interface TrainRepository extends ReadOnlyRepository<Train, Long> {
    @Query("SELECT a FROM Train a JOIN FETCH a.startDates")
    Set<Train> findAllWithStartDates();
}
