package poweska.app.service;

import org.springframework.stereotype.Service;
import poweska.app.domain.*;
import poweska.app.dto.StartDateDTO;
import poweska.app.dto.TrainDTO;
import poweska.app.util.RailLineIterator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Service
public class ScheduleService {
    private final Map<Long, Train> trainMap;
    private final Map<String, Station> stationMap;

    public ScheduleService(Map<Long, Train> trainMap, Map<String, Station> stationMap) {
        this.trainMap = trainMap;
        this.stationMap = stationMap;
    }

    public List<TrainDTO> createSchedule(String stationName) {
        List<TrainDTO> result = new ArrayList<>();
        Station station = stationMap.get(stationName);

        for (Train train : trainMap.values()) {
            int duration = 0;
            RailLine line = train.getLine();
            RailLineIterator iterator = new RailLineIterator(line);

            while (!iterator.isChangePoint(station)) {
                duration += iterator.getNext().getDuration();
            }

            if (iterator.hasNext()) {
                List<String> stops = new ArrayList<>();
                LineFragment first = iterator.getNext();
                int startPlatform = first.getStartPlatform();

                while (iterator.hasNext()) {
                    stops.add(first.getEndName());
                    first = iterator.getNext();
                }

                List<StartDateDTO> dates = train.calculateStartDates(duration);
                dates.sort(Comparator.comparing(StartDateDTO::getDayOfWeek));

                result.add(TrainDTO.builder()
                        .trainName(train.getName())
                        .fromPlatform(startPlatform)
                        .toStation(first.getEndName())
                        .startDates(dates)
                        .stops(stops)
                        .build());
            }
        }

        result.sort(Comparator.comparing(TrainDTO::getToStation));
        return result;
    }

}
