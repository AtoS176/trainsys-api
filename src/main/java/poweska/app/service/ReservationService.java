package poweska.app.service;

import org.springframework.stereotype.Service;
import poweska.app.domain.Buyer;
import poweska.app.domain.Seat;
import poweska.app.domain.Ticket;
import poweska.app.dto.TicketDTO;
import poweska.app.repository.crud.BuyerRepository;
import poweska.app.repository.crud.TicketRepository;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    private final SeatService seatService;
    private final EmailService emailService;
    private final BuyerRepository buyerRepository;
    private final TicketRepository ticketRepository;

    public ReservationService(SeatService seatService, EmailService emailService,
                              BuyerRepository buyerRepository, TicketRepository ticketRepository) {

        this.seatService = seatService;
        this.emailService = emailService;
        this.buyerRepository = buyerRepository;
        this.ticketRepository = ticketRepository;
    }

    public void reserveTicket(TicketDTO ticketDTO) {
        Buyer buyer = buyerRepository.findByEmail(ticketDTO.getEmail());
        if (buyer == null) {
            buyer = buyerRepository.save(new Buyer(ticketDTO.getEmail()));
        }

        List<Seat> reservedSeats = seatService.createSeatsFromTicketDTO(ticketDTO);
        Ticket ticket = ticketRepository.save(new Ticket(reservedSeats, buyer));
        reservedSeats.forEach(seat -> seat.setTicket(ticket));
        seatService.saveAll(reservedSeats);
        emailService.sendTicket(ticket, ticketDTO.getEmail());
    }

    public boolean cancelTicket(String email, Long ticketId, String code) {
        Ticket ticket = ticketRepository.findByIdAndCode(ticketId, code);
        Buyer buyer = buyerRepository.findByEmail(email);

        if (ticket == null || buyer == null) {
            return false;
        }

        if (buyer.getTicketSet().contains(ticket)) {
            Optional<LocalDateTime> acceptedDates = ticket.getReservedSeat()
                    .stream()
                    .map(seat -> seat.calculateTime(Seat.ContextType.FROM))
                    .filter(dateTime -> dateTime.isAfter(LocalDateTime.now().plusHours(24)))
                    .findAny();

            if (acceptedDates.isPresent()) {
                try {
                    emailService.sendCancellation(email, code);
                } catch (MessagingException e) {
                    e.getMessage();
                    return false;
                }
                ticketRepository.delete(ticket);
                return true;
            }
        }

        return false;
    }

}
