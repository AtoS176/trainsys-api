package poweska.app.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import poweska.app.domain.Station;
import poweska.app.domain.Train;
import poweska.app.util.staticMethod.PathReducer;
import poweska.app.util.finalObject.ConnectionCreator;
import poweska.app.util.finalObject.Edge;
import poweska.app.dto.ConnectionDTO;
import poweska.app.util.finalObject.Dfs;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GraphService {
    private final Map<Long, Train> trainMap;
    private final Map<String, Station> stationMap;
    private final Dfs dfs;

    public GraphService(Map<Long, Train> trainMap, Map<String, Station> stationMap,Dfs dfs) {
        this.trainMap = trainMap;
        this.stationMap = stationMap;
        this.dfs = dfs;
    }

    @Cacheable(value = "connections")
    public List<ConnectionDTO> findConnections(String from, String to, LocalDateTime when) {
        if(from.equals(to)){
            return Collections.emptyList();
        }

        final Station fromStation = stationMap.get(from);
        final Map<Train, Pair<Integer, ConnectionDTO>> connectionMap = new HashMap<>();

        List<List<Edge>> paths = dfs.findPaths(from, to, when.toLocalDate());

        paths.forEach(path -> {
                List<Train> trains = new ArrayList<>();
                List<Station> stations = new ArrayList<>(Collections.singletonList(fromStation));

                for (Edge edge : PathReducer.reduce(path)) {
                    trains.add(trainMap.get(edge.getTrainId()));
                    stations.add(stationMap.get(edge.getToStation()));
                }

                if (trains.size() != 0) {
                    Train key = trains.get(0);
                    if (!connectionMap.containsKey(key) || connectionMap.get(key).getFirst() > trains.size()) {
                        ConnectionCreator creator = new ConnectionCreator(stations, trains);
                        Optional<ConnectionDTO> connect = creator.create(when);
                        connect.ifPresent(value -> connectionMap.put(key, Pair.of(trains.size(), value)));
                    }
                }
        });

        List<ConnectionDTO> result = connectionMap.values()
                .stream()
                .map(Pair::getSecond)
                .sorted(Comparator.comparing(ConnectionDTO::getDistance, Integer::compareTo))
                .collect(Collectors.toList());

        result.removeIf(value -> value.getDistance() > result.get(0).getDistance() * 1.5);
        result.sort(Comparator.comparing(ConnectionDTO::getStartDate, LocalDateTime::compareTo));
        return result;
    }

}

