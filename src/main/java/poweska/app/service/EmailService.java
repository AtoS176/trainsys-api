package poweska.app.service;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import poweska.app.domain.Ticket;
import poweska.app.util.EmailBuilder;
import poweska.app.util.staticMethod.PdfGenerator;

import javax.mail.MessagingException;

@Service
public class EmailService {
    private final JavaMailSender mailSender;

    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Async
    public void sendTicket(Ticket ticket, String email) {
        EmailBuilder.of(mailSender)
                .map(build -> build.withAddressee(email))
                .map(build -> build.withSubject("Polska Kolej - Rezerwacja Biletu"))
                .map(build -> build.withText("Dziękujemy za skorzystanie z naszych usług. W załączniku przesyłamy kupiony bilet."))
                .map(build -> build.withAttachment(PdfGenerator.generateTicket(ticket)))
                .flatMap(EmailBuilder::build)
                .ifPresent(mailSender::send);
    }

    @Async
    public void sendCancellation(String email, String code) throws MessagingException {
        EmailBuilder.of(mailSender)
                .map(build -> build.withAddressee(email))
                .map(build -> build.withSubject("Polska Kolej - Anulowanie Biletu"))
                .map(build -> build.withText(String.format("Twój bilet o kodzie %s został anulowany", code)))
                .flatMap(EmailBuilder::build)
                .ifPresent(mailSender::send);
    }

}
