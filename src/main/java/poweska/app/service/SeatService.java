package poweska.app.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import poweska.app.domain.Seat;
import poweska.app.domain.Station;
import poweska.app.domain.Train;
import poweska.app.dto.ReservationDTO;
import poweska.app.domain.SeatType;
import poweska.app.dto.SeatDTO;
import poweska.app.dto.TicketDTO;
import poweska.app.repository.crud.SeatRepository;
import poweska.app.repository.readOnly.StationRepository;
import poweska.app.repository.readOnly.TrainRepository;
import poweska.app.util.staticMethod.SeatFinder;
import poweska.app.util.finalObject.BoundaryFragments;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

@Service
public class SeatService {
    private final SeatRepository seatRepository;
    private final TrainRepository trainRepository;
    private final StationRepository stationRepository;

    public SeatService(SeatRepository seatRepository, TrainRepository trainRepository, StationRepository stationRepository) {
        this.seatRepository = seatRepository;
        this.trainRepository = trainRepository;
        this.stationRepository = stationRepository;
    }

    public void saveAll(List<Seat> seats) {
        seatRepository.saveAll(seats);
    }

    public Set<Seat> findByTrainIdAndDate(Long trainId, LocalDate when) {
        return seatRepository.findByTrainIdAndDate(trainId, when);
    }

    @Transactional
    public List<Integer> findAvailableTrucks(ReservationDTO reservation) {
        ListIterator<String> stationsIter = reservation.getStations().listIterator();
        List<Integer> trucksNumber = new ArrayList<>();

        for (Long trainId : reservation.getTrainsId()) {
            Train train = trainRepository.getOne(trainId);
            Station from = stationRepository.findByName(stationsIter.next());
            Station to = stationRepository.findByName(stationsIter.next());

            BoundaryFragments boundaries = new BoundaryFragments(train.getLine(), from, to);
            Set<Seat> reservedInTrain = findByTrainIdAndDate(trainId, reservation.getWhen());
            Map<Integer, List<Seat>> busySeats = SeatFinder.findBusySeats(reservedInTrain, boundaries);
            stationsIter.previous();

            try {
                trucksNumber.add(train.findAvailableTruck(busySeats, reservation.getSeatTypes().size()));
            } catch (NoSuchElementException e) {
                return Collections.emptyList();
            }

        }
        return trucksNumber;
    }

    @Transactional
    public Optional<TicketDTO> findSeatsToReserve(ReservationDTO reservation) {
        Iterator<Integer> trucksIter = findAvailableTrucks(reservation).iterator();
        if (!trucksIter.hasNext()) {
            return Optional.empty();
        }

        List<SeatDTO> seats = new ArrayList<>();
        Iterator<String> stationIter = reservation.getStations().iterator();
        String from = stationIter.next();

        for (Long trainId : reservation.getTrainsId()) {
            String to = stationIter.next();

            Station fromStation = stationRepository.findByName(from);
            Station toStation = stationRepository.findByName(to);
            Train train = trainRepository.getOne(trainId);
            int truck = trucksIter.next();
            BigDecimal basicPrice = train.getLine().calculatePrice(fromStation, toStation);

            for (SeatType seatType : reservation.getSeatTypes()) {
                SeatDTO seat = SeatDTO.builder()
                        .trainId(trainId)
                        .trainName(train.getName())
                        .startTime(train.calculateTime(fromStation, reservation.getWhen()))
                        .endTime(train.calculateTime(toStation, reservation.getWhen()))
                        .truckNumber(truck)
                        .startStation(fromStation.getName())
                        .endStation(toStation.getName())
                        .seatType(seatType)
                        .price(basicPrice
                                .multiply(BigDecimal.valueOf(1 - seatType.getDiscount()))
                                .setScale(2, RoundingMode.HALF_UP)
                                .doubleValue())
                        .build();
                seats.add(seat);
            }
            from = to;
        }

        return Optional.of(new TicketDTO(seats, reservation.getEmail()));
    }

    public List<Seat> createSeatsFromTicketDTO(TicketDTO ticketDTO) {
        List<Seat> seats = new ArrayList<>();

        for (SeatDTO seatDTO : ticketDTO.getSeatsInTrains()) {
            Station from = stationRepository.findByName(seatDTO.getStartStation());
            Station to = stationRepository.findByName(seatDTO.getEndStation());
            Train train = trainRepository.getOne(seatDTO.getTrainId());
            BigDecimal basicPrice = train.getLine().calculatePrice(from, to);
            seats.add(Seat.builder()
                    .seatType(seatDTO.getSeatType())
                    .date(seatDTO.getStartTime().toLocalDate())
                    .truckNumber(seatDTO.getTruckNumber())
                    .from(from)
                    .to(to)
                    .train(train)
                    .price(basicPrice
                            .multiply(BigDecimal.valueOf(1 - seatDTO.getSeatType().getDiscount()))
                            .setScale(2, RoundingMode.HALF_UP))
                    .build());
        }
        return seats;
    }

}
