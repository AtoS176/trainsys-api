package poweska.app.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import poweska.app.domain.Station;
import poweska.app.domain.Train;
import poweska.app.repository.readOnly.StationRepository;
import poweska.app.repository.readOnly.TrainRepository;
import poweska.app.util.finalObject.Dfs;
import poweska.app.util.finalObject.Edge;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
public class MemoryCache {

    @Bean
    public Map<String, Station> stationMap(StationRepository stationRepository){
        return stationRepository
                .findAll()
                .stream()
                .collect(Collectors.toUnmodifiableMap(Station::getName, Function.identity()));
    }

    @Bean
    public Map<Long, Train> trainMap(TrainRepository trainRepository){
        return trainRepository
                .findAllWithStartDates()
                .stream()
                .collect(Collectors.toUnmodifiableMap(Train::getId, Function.identity()));
    }

    @Bean
    public Map<String, List<Edge>> graphMap(TrainRepository trainRepository){
        Map<String, List<Edge>> graph = new HashMap<>();

        trainRepository.findAllWithStartDates()
                .forEach(trainDo -> trainDo.getLine().getFragments()
                        .forEach(lineFragment -> {
                            String key = lineFragment.getStartName();
                            Edge edge = new Edge(trainDo.getId(), lineFragment.getEndName());
                            if (!graph.containsKey(key)) {
                                graph.put(key, new ArrayList<>(Collections.singletonList(edge)));
                            } else {
                                List<Edge> actualValues = graph.get(key);
                                actualValues.add(edge);
                                graph.put(key, actualValues);
                            }
                        }));

        return Collections.unmodifiableMap(graph);
    }

    @Bean
    public Dfs dfs(Map<String, Station> stationMap, Map<Long, Train> trainMap, Map<String, List<Edge>> graphMap){
        return new Dfs(trainMap, stationMap, graphMap);
    }

}
