package poweska.app.util;

import poweska.app.domain.LineFragment;
import poweska.app.domain.RailLine;
import poweska.app.domain.Station;

public class RailLineIterator implements FragmentIterator {
    private final RailLine railLine;
    private int currIndex = 0;

    public RailLineIterator(RailLine railLine){
        this.railLine = railLine;
    }

    @Override
    public boolean hasNext() {
        return currIndex < railLine.getFragments().size();
    }

    @Override
    public LineFragment getNext() {
        if(!this.hasNext()){
            return null;
        }

        LineFragment fragment = railLine.getFragments().get(currIndex);
        currIndex++;
        return fragment;
    }

    @Override
    public boolean isChangePoint(Station station) {
        if(!hasNext()){
            return true;
        }

        return railLine.getFragments()
                .get(currIndex)
                .getStart()
                .equalsByStation(station);
    }

}
