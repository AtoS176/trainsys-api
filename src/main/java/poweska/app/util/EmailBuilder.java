package poweska.app.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.HashMap;
import java.util.Optional;

public class EmailBuilder {
    private final static Logger logger = LoggerFactory.getLogger(EmailBuilder.class);
    private final MimeMessage message;
    private final MimeMessageHelper helper;
    private final HashMap<String, String> args = new HashMap<>();

    private EmailBuilder(MimeMessage message, MimeMessageHelper helper) {
        this.message = message;
        this.helper = helper;
    }

    public static Optional<EmailBuilder> of(JavaMailSender mailSender) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            return Optional.of(new EmailBuilder(message, helper));
        } catch (MessagingException ex) {
            logger.error("Error while creating email builder. Exception {}", ex.getMessage());
            return Optional.empty();
        }
    }

    public EmailBuilder withAddressee(String email) {
        args.put("email", email);
        return this;
    }

    public EmailBuilder withSubject(String subject) {
        args.put("subject", subject);
        return this;
    }

    public EmailBuilder withText(String text) {
        args.put("text", text);
        return this;
    }

    public EmailBuilder withAttachment(String path) {
        args.put("attachmentPath", path);
        return this;
    }

    public Optional<MimeMessage> build() {
        try {
            if (args.containsKey("attachmentPath")) {
                FileSystemResource file = new FileSystemResource(new File(args.get("attachmentPath")));
                helper.addAttachment("bilet.pdf", file);
            }
            helper.setTo(args.get("email"));
            helper.setSubject(args.get("subject"));
            helper.setText(args.get("text"));
            return Optional.of(message);
        } catch (MessagingException ex) {
            logger.error("Error while creating email. Exception {}", ex.getMessage());
            return Optional.empty();
        }
    }

}
