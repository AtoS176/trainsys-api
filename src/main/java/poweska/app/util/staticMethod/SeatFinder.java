package poweska.app.util.staticMethod;

import poweska.app.domain.Seat;
import poweska.app.util.finalObject.BoundaryFragments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public final class SeatFinder {
    private SeatFinder(){};

    public static Map<Integer, List<Seat>> findBusySeats(Set<Seat> seatsInTrain, BoundaryFragments boundaries){
        return seatsInTrain.stream()
                .filter(seat -> seat.isBusy(boundaries))
                .collect(Collectors.groupingBy(Seat::getTruckNumber, HashMap::new, toList()));
    }

}
