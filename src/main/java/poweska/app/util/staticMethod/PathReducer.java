package poweska.app.util.staticMethod;

import poweska.app.util.finalObject.Edge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class PathReducer {
    private static final int MAX_REDUCE_EDGE = 3;

    private PathReducer(){};

    public static List<Edge> reduce(List<Edge> path) {
        if (path.size() == 1) {
            return path;
        }

        List<Edge> reduced = new ArrayList<>();
        Edge previous = path.get(0);
        Edge next = null;

        for (int i = 1; i < path.size(); i++) {
            next = path.get(i);
            if (!previous.equalByTrain(next)) {
                reduced.add(previous);
            }
            previous = next;
        }

        reduced.add(next);
        return reduced.size() - 1 < MAX_REDUCE_EDGE ? reduced : Collections.emptyList();
    }

}
