package poweska.app.util.staticMethod;

import java.util.List;

public final class EmptyListChecker {
    private EmptyListChecker() {};

    public static <T> boolean isNullOrEmpty(List<T> objects) {
        return objects == null || objects.size() == 0;
    }

}
