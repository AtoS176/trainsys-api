package poweska.app.util.staticMethod;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import poweska.app.domain.Seat;
import poweska.app.domain.Ticket;
import poweska.app.domain.Train;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public final  class PdfGenerator {
    private static final Font font = setFont();
    private static final String resource = "src/main/resources";
    private static final String [] tableHeader = {
            "Pasażer",
            "Odjazd",
            "Przyjazd",
            "Skąd (Peron)",
            "Dokąd (Peron)",
            "Wagon", "Typ",
            "Cena"
    };

    private PdfGenerator(){};

    public static String generateTicket(Ticket ticket){
        Document document = new Document(PageSize.A4.rotate());
        try {
            PdfWriter.getInstance(document, new FileOutputStream(ticket.getId() + ".pdf"));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }

        document.open();
        addHeader(document);
        boolean added = addData(document, ticket);
        document.close();

        return added ? ticket.getId() + ".pdf" : null;
    }

    private static void addHeader(Document document){
        Image image = null;
        try {
            image = Image.getInstance(resource + "/static/icon.png");
            image.scaleToFit(document.getPageSize());
        } catch (BadElementException | IOException e) {
            e.getMessage();
        }

        Paragraph paragraph = new Paragraph();
        paragraph.add(image);
        try {
            document.add(paragraph);
            document.add(Chunk.NEWLINE);
        } catch (DocumentException e) {
            e.getMessage();
        }
    }

    private static boolean addData(Document document, Ticket ticket){
        Map<Train, List<Seat>> seatsGroupedByTrain = ticket.getReservedSeat().stream()
                .collect(groupingBy(Seat::getTrain, LinkedHashMap::new, toList()));

        try {
            for(Map.Entry<Train, List<Seat>> entrySet : seatsGroupedByTrain.entrySet()){
                document.add(new Chunk(entrySet.getKey().toString(), font));

                PdfPTable table = new PdfPTable(new float[]{2, 4, 4, 4, 4, 2, 3, 2});
                Stream.of(tableHeader).forEach(column -> table.addCell(createCell(column)));
                table.setWidthPercentage(100);

                for(int i = 0; i < entrySet.getValue().size(); i++){
                    addRow(i+1, entrySet.getValue().get(i), table);
                }

                document.add(table);
                document.add(Chunk.NEWLINE);
            }
            String idText = "Identyfikator Biletu : " + ticket.getId();
            String priceText = "Cena : " + ticket.getPriceContext();
            document.add(createParagraph(idText, priceText, 150));

            String codeText = "Kod Biletu : " + ticket.getCode();
            String dateText = "Data zakupu : " + LocalDate.now();
            document.add(createParagraph(codeText, dateText, 150 + idText.length() - codeText.length()));
        } catch (DocumentException e) {
            e.getMessage();
            return false;
        }

        return true;
    }

    private static void addRow(int passengerNo, Seat seat, PdfPTable table) {
        table.addCell(createCell(String.valueOf(passengerNo)));
        table.addCell(createCell(seat.getTimeContext(Seat.ContextType.FROM)));
        table.addCell(createCell(seat.getTimeContext(Seat.ContextType.TO)));
        table.addCell(createCell(seat.getStationContext(Seat.ContextType.FROM)));
        table.addCell(createCell(seat.getStationContext(Seat.ContextType.TO)));
        table.addCell(createCell(String.valueOf(seat.getTruckNumber())));
        table.addCell(createCell(seat.getSeatType().name()));
        table.addCell(createCell(seat.getPriceContext()));
    }

    private static PdfPCell createCell(String context) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(context, font));
        return cell;
    }

    private static Paragraph createParagraph(String first, String second, int margin) {
        Paragraph p = new Paragraph();
        p.setFont(font);
        p.add(String.format("%s %"+ margin + "s", first, second));
        p.setSpacingAfter(1.2f);
        return p;
    }

    private static Font setFont() {
        final int fontSize = 13;
        Font font = FontFactory.getFont(FontFactory.HELVETICA, fontSize);
        try {
            BaseFont myFont = BaseFont.createFont(resource + "/static/Arial.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            font = new Font(myFont, fontSize);
        } catch (IOException | DocumentException e) {
            e.getMessage();
        }
        return font;
    }

}
