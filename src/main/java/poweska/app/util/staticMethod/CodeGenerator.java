package poweska.app.util.staticMethod;

import java.security.SecureRandom;
import java.util.Base64;

public final class CodeGenerator {
    private static final int length = 4;

    private CodeGenerator(){};

    public static String generate(){
        SecureRandom random = new SecureRandom();
        byte [] code = new byte[length];
        random.nextBytes(code);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(code);
    }

}
