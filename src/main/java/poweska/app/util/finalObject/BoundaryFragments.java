package poweska.app.util.finalObject;

import lombok.Getter;
import poweska.app.domain.LineFragment;
import poweska.app.domain.RailLine;
import poweska.app.domain.Station;

import java.util.NoSuchElementException;

@Getter
public class BoundaryFragments {
    private final int beginNumber;
    private final int lastNumber;

    public BoundaryFragments(RailLine railLine, Station begin, Station last) {
        beginNumber = calculateBegin(railLine, begin);
        lastNumber = calculateLast(railLine, last);
    }

    private int calculateBegin(RailLine railLineDO, Station begin){
        return railLineDO.getFragments().stream()
                .filter(lineFragment -> lineFragment.isStartStation(begin))
                .map(LineFragment::getFragmentNumber)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    private int calculateLast(RailLine railLineDO, Station last){
        return railLineDO.getFragments().stream()
                .filter(lineFragment -> lineFragment.isEndStation(last))
                .map(LineFragment::getFragmentNumber)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    public boolean hasOverlap(BoundaryFragments reserved){
        if(beginNumber >= reserved.getLastNumber()){
            return false;
        }
        return reserved.beginNumber <= lastNumber;
    }

}
