package poweska.app.util.finalObject;

import org.springframework.data.util.Pair;
import poweska.app.domain.Station;
import poweska.app.domain.Train;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Dfs {
    private final Map<Long, Train> trains;
    private final Map<String, Station> stations;
    private final Map<String, List<Edge>> graph;

    public Dfs(Map<Long, Train> trains, Map<String, Station> stations, Map<String, List<Edge>> graph) {
        this.trains = trains;
        this.stations = stations;
        this.graph = graph;
    }

    public List<List<Edge>> findPaths(String start, String end, LocalDate date) {
        DfsRunner dfsRunner = new DfsRunner(end, date);
        dfsRunner.run(start, null, new LinkedList<>());
        return dfsRunner.paths;
    }

    private class DfsRunner {
        private final static int MAX_WAITING_TIME = 120;
        private final Map<String, Boolean> visited = new HashMap<>();
        private final List<List<Edge>> paths = new ArrayList<>();
        private final LocalDate date;
        private final String end;

        public DfsRunner(String end, LocalDate date) {
            this.end = end;
            this.date = date;
        }

        public void run(String start, LocalDateTime actualTime, List<Edge> path) {
            visited.put(start, true);

            if (start.equals(end)) {
                paths.add(new LinkedList<>(path));
                visited.put(start, false);
                return;
            }

            if (graph.get(start) != null) {
                for (Edge edge : graph.get(start)) {
                    if (!visited.containsKey(edge.getToStation()) || !visited.get(edge.getToStation())) {
                        Pair<Boolean, LocalDateTime> availability = checkAvailability(start, edge, actualTime);
                        if (availability.getFirst()) {
                            path.add(edge);
                            this.run(edge.getToStation(), availability.getSecond(), path);
                            path.remove(edge);
                        }
                    }
                }
            }
            visited.put(start, false);
        }

        private Pair<Boolean, LocalDateTime> checkAvailability(String start, Edge edge, LocalDateTime time){
            Train train = trains.get(edge.getTrainId());
            LocalDateTime departure = train.calculateTime(stations.get(start), date);
            LocalDateTime arrival = train.calculateTime(stations.get(edge.getToStation()), date);

            if(time == null){
                return Pair.of(true, arrival);
            }else{
                boolean isAfter = !departure.isBefore(time);
                boolean isBeforeLimit = departure.isBefore(time.plusMinutes(MAX_WAITING_TIME));
                if(isAfter && isBeforeLimit){
                    return Pair.of(true, arrival);
                }
            }

            return Pair.of(false, arrival);
        }

    }

}
