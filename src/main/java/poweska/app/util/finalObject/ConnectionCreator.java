package poweska.app.util.finalObject;

import poweska.app.dto.ConnectionDTO;
import poweska.app.dto.FragmentDTO;
import poweska.app.domain.StartDate;
import poweska.app.domain.Station;
import poweska.app.domain.Train;

import java.time.LocalDateTime;
import java.util.*;

import java.util.List;

public class ConnectionCreator {
    private final ListIterator<Station> stationsIter;
    private final List<Train> trains;

    public ConnectionCreator(List<Station> stations, List<Train> trains) {
        this.stationsIter = stations.listIterator();
        this.trains = trains;
    }

    public Optional<ConnectionDTO> create(LocalDateTime date){
        List<FragmentDTO> fragments = new LinkedList<>();

        for(Train train : trains){
            StartDate startDate = train.findProperStartDates(date.toLocalDate());
            date = date.with(startDate.convertToLocalTime());
            FragmentsCreator creator = new FragmentsCreator(train, stationsIter.next(), stationsIter.next());
            fragments.addAll(creator.create(date));
            stationsIter.previous();
        }

        double price = calculatePrice(fragments);
        int distance = calculateDistance(fragments);

        return Optional.of(new ConnectionDTO(fragments, distance, price));
    }

    public int calculateDistance(List<FragmentDTO> allFragments){
        return allFragments.stream()
                .map(FragmentDTO::getDistance)
                .reduce(Integer::sum)
                .orElse(0);
    }

    public double calculatePrice(List<FragmentDTO> allFragments){
        return allFragments.stream()
                .map(FragmentDTO::getPrice)
                .reduce(Double::sum)
                .orElse(0.0);
    }

}
