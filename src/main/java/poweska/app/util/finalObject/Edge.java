package poweska.app.util.finalObject;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class Edge {
    private final Long trainId;
    private final String toStation;

    public Edge(Long trainId, String toStation) {
        this.trainId = trainId;
        this.toStation = toStation;
    }

    public boolean equalByTrain(Edge edge){
        return trainId.equals(edge.getTrainId());
    }

    public boolean equalByStation(Edge edge){
        return toStation.equals(edge.toStation);
    }

    public boolean isMyStation(String station){
        return toStation.equals(station);
    }

}
