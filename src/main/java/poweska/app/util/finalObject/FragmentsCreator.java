package poweska.app.util.finalObject;

import poweska.app.dto.FragmentDTO;
import poweska.app.domain.LineFragment;
import poweska.app.domain.Station;
import poweska.app.domain.Train;
import poweska.app.util.RailLineIterator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FragmentsCreator {
    private final Train train;
    private final Station start;
    private final Station end;

    public FragmentsCreator(Train train, Station start, Station end) {
        this.train = train;
        this.start = start;
        this.end = end;
    }

    public List<FragmentDTO> create(LocalDateTime date){
        List<FragmentDTO> fragments = new ArrayList<>();
        var lineIter = new RailLineIterator(train.getLine());

        while(!lineIter.isChangePoint(start)){
            date = date.plusMinutes(lineIter.getNext().getDuration());
        }

        LineFragment fragment;
        while(!lineIter.isChangePoint(end)){
            fragment = lineIter.getNext();
            var fragmentDTO = FragmentDTO.builder()
                    .id(train.getId())
                    .trainName(train.getName())
                    .startStation(fragment.getStartName())
                    .endStation(fragment.getEndName())
                    .startPlatform(fragment.getStartPlatform())
                    .endPlatform(fragment.getEndPlatform())
                    .departure(date)
                    .arrival(date.plusMinutes(fragment.getDuration()))
                    .distance(fragment.getDistance())
                    .price(fragment.getPrice().doubleValue())
                    .build();

            date = date.plusMinutes(fragment.getDuration());
            fragments.add(fragmentDTO);
        }

        return fragments;
    }

}
