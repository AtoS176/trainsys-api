package poweska.app.util;


import poweska.app.domain.LineFragment;
import poweska.app.domain.Station;

public interface FragmentIterator {
    boolean hasNext();
    LineFragment getNext();
    boolean isChangePoint(Station station);
}
