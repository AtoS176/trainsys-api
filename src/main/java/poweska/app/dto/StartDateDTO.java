package poweska.app.dto;

import lombok.Data;
import poweska.app.domain.StartDate;

import java.io.Serializable;
import java.time.DayOfWeek;

@Data
public class StartDateDTO implements Serializable {
    private DayOfWeek dayOfWeek;
    private int hour;
    private int minute;

    public StartDateDTO(StartDate startDate){
        this.dayOfWeek = startDate.getDayOfWeek();
        this.hour = startDate.getHour();
        this.minute = startDate.getMinute();
    }

}
