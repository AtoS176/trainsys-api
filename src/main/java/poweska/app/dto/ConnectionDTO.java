package poweska.app.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ConnectionDTO implements Serializable {
    private final List<FragmentDTO> fragments;
    private final int distance;
    private final double price;

    @JsonBackReference
    public LocalDateTime getStartDate(){
        return fragments.get(0).getDeparture();
    }

}
