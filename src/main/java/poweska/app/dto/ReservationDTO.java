package poweska.app.dto;

import lombok.Builder;
import lombok.Data;
import poweska.app.domain.SeatType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class ReservationDTO implements Serializable {
    private List<Long> trainsId;
    private List<SeatType> seatTypes;
    private List<String> stations;
    private LocalDate when;
    private String email;
}
