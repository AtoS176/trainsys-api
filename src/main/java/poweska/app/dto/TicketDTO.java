package poweska.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class TicketDTO implements Serializable {
    private List<SeatDTO> seatsInTrains;
    private String email;
}
