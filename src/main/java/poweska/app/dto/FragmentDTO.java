package poweska.app.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.jni.Local;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class FragmentDTO implements Serializable {
    private Long id;
    private String trainName;
    private LocalDateTime departure;
    private String startStation;
    private int startPlatform;
    private LocalDateTime arrival;
    private String endStation;
    private int endPlatform;
    private int distance;
    private double price;
}
