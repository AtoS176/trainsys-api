package poweska.app.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class TrainDTO implements Serializable {
    private String trainName;
    private String toStation;
    private List<String> stops;
    private List<StartDateDTO> startDates;
    private int fromPlatform;
}
