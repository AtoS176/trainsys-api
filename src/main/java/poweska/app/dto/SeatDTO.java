package poweska.app.dto;

import lombok.Builder;
import lombok.Data;
import poweska.app.domain.SeatType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class SeatDTO implements Serializable {
    private double price;
    private String trainName;
    private Long trainId;
    private int truckNumber;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String startStation;
    private String endStation;
    private SeatType seatType;
}
