package poweska.app.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import poweska.app.util.finalObject.BoundaryFragments;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Builder
@AllArgsConstructor
@Entity
@Table(name = "seat")
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price", precision = 10, scale = 2)
    private BigDecimal price;

    @Column(name = "seat_type")
    @Enumerated(EnumType.STRING)
    private SeatType seatType;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "truck_number")
    private int truckNumber;

    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train train;

    @ManyToOne
    @JoinColumn(name = "from_id")
    private Station from;

    @ManyToOne
    @JoinColumn(name = "to_id")
    private Station to;

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    public enum ContextType {FROM, TO}

    public Seat() {
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public boolean isBusy(BoundaryFragments toReserve) {
        return toReserve.hasOverlap(new BoundaryFragments(train.getLine(), from, to));
    }

    public String getPriceContext() {
        return price + " zł";
    }

    public String getStationContext(ContextType type) {
        return type.equals(ContextType.FROM) ?
                from.getName() + "(" + train.getLine().findStartPlatform(from) + ")" :
                to.getName() + "(" + train.getLine().findEndPlatform(to) + ")";
    }

    public String getTimeContext(ContextType type) {
        LocalDateTime dateTime = calculateTime(type);
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    public LocalDateTime calculateTime(ContextType type) {
        return type.equals(ContextType.FROM) ?
                train.calculateTime(from, date) :
                train.calculateTime(to, date);
    }

}
