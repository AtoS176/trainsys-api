package poweska.app.domain;

import lombok.Getter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Entity
@Table(name = "buyer")
public class Buyer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "buyer")
    private Set<Ticket> ticketSet;

    public Buyer() {
    }

    public Buyer(String email) {
        this.email = email;
    }

}
