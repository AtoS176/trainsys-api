package poweska.app.domain;

import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@Immutable
@Table(name = "station")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "station")
    private Set<StationDetails> stationDetails;

    @OneToMany(mappedBy = "from")
    private Set<Seat> fromReservedSeat;

    @OneToMany(mappedBy = "to")
    private Set<Seat> toReservedSeat;

    public Station() {
    }

    public Station(Long id, String name) {
        this.name = name;
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }

        Station station = (Station) obj;
        return station.getId().equals(this.id) && station.getName().equals(this.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}
