package poweska.app.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import poweska.app.util.staticMethod.CodeGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Getter
@EqualsAndHashCode
@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price", precision = 10, scale = 2)
    private BigDecimal price;

    @Column(name = "code")
    private String code = CodeGenerator.generate();

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<Seat> reservedSeat;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    private Buyer buyer;

    public Ticket() {
    }

    public Ticket(List<Seat> reservedSeat, Buyer buyer) {
        this.reservedSeat = reservedSeat;
        this.buyer = buyer;
        this.price = calculateTicketPrice();
    }

    private BigDecimal calculateTicketPrice() {
        return reservedSeat.stream()
                .map(Seat::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
    }

    public String getPriceContext() {
        return price + " zł";
    }

}
