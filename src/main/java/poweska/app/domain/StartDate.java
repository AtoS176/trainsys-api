package poweska.app.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;

@Getter
@Entity
@Builder
@AllArgsConstructor
@Immutable
@Table(name = "start_date")
public class StartDate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "day_of_week")
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

    @Column(name = "hour")
    private int hour;

    @Column(name = "minute")
    private int minute;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "train_start_date",
            joinColumns = {@JoinColumn(name = "start_date_id")},
            inverseJoinColumns = {@JoinColumn(name = "train_id")}
    )
    private Set<Train> trains;

    public StartDate() {
    }

    public boolean equalsByDay(DayOfWeek day) {
        return dayOfWeek.equals(day);
    }

    public LocalDateTime convertToLocalDateTime(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.of(hour, minute));
    }

    public LocalTime convertToLocalTime() {
        return LocalTime.of(hour, minute);
    }

    public StartDate plusMinutes(int minute) {
        int allMinutes = this.minute + 60 * this.hour + minute;
        int newMin = allMinutes % 60;
        int newHour = (allMinutes / 60) % 24;
        DayOfWeek newDay = this.dayOfWeek.plus(allMinutes / 60 / 24);

        return StartDate.builder()
                .dayOfWeek(newDay)
                .hour(newHour)
                .minute(newMin)
                .build();
    }

}
