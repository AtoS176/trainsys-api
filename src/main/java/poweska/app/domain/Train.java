package poweska.app.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;
import poweska.app.dto.StartDateDTO;
import poweska.app.util.RailLineIterator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Entity
@Builder
@AllArgsConstructor
@Immutable
@Table(name = "train")
public class Train {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "trucks_number")
    private int trucksNumber;

    @Column(name = "place_in_truck")
    private int placeInTruck;

    @OneToOne
    @JoinColumn(name = "line_id")
    private RailLine line;

    @ManyToMany(mappedBy = "trains")
    private Set<StartDate> startDates;

    @OneToMany(mappedBy = "train")
    private Set<Seat> reservedSeats;

    public Train() {
    }

    public LocalDateTime calculateTime(Station station, LocalDate date) {
        if (findProperStartDates(date) == null) {
            throw new NoSuchElementException("Date is unavailable : " + date);
        }

        LocalDateTime start = findProperStartDates(date).convertToLocalDateTime(date);
        var lineIterator = new RailLineIterator(line);
        while (!lineIterator.isChangePoint(station)) {
            start = start.plusMinutes(lineIterator.getNext().getDuration());
        }

        return start;
    }

    public List<StartDateDTO> calculateStartDates(int tripDuration) {
        return startDates.stream()
                .map(startDates -> startDates.plusMinutes(tripDuration))
                .map(StartDateDTO::new)
                .collect(Collectors.toList());
    }

    public StartDate findProperStartDates(LocalDate when) {
        return startDates.stream()
                .filter(stDate -> stDate.equalsByDay(when.getDayOfWeek()))
                .findFirst()
                .orElse(null);
    }

    public int findAvailableTruck(Map<Integer, List<Seat>> busySeats, int seats) {
        for (int truckNo = 1; truckNo <= trucksNumber; truckNo++) {
            if (placeInTruck >= seats) {
                List<Seat> busyInTruck = busySeats.get(truckNo);
                if (busyInTruck == null || placeInTruck - busyInTruck.size() >= seats) {
                    return truckNo;
                }
            }
        }
        throw new NoSuchElementException("All trucks are busy");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }

        Train train = (Train) obj;
        return train.getId().equals(this.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Pociąg " + name;
    }

}
