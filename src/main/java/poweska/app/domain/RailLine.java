package poweska.app.domain;

import lombok.Getter;
import org.hibernate.annotations.Immutable;
import poweska.app.util.RailLineIterator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Getter
@Entity
@Immutable
@Table(name = "rail_line")
public class RailLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(mappedBy = "line")
    private Train train;

    @OneToMany(mappedBy = "line", fetch = FetchType.EAGER)
    @OrderBy(value = "fragmentNumber")
    private List<LineFragment> fragments;

    public RailLine() {
    }

    public RailLine(List<LineFragment> fragments) {
        this.fragments = fragments;
    }

    public int findStartPlatform(Station station) {
        return fragments.stream()
                .filter(fragment -> fragment.isStartStation(station))
                .map(LineFragment::getStartPlatform)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    public int findEndPlatform(Station station) {
        return fragments.stream()
                .filter(fragment -> fragment.isEndStation(station))
                .map(LineFragment::getEndPlatform)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    public BigDecimal calculatePrice(Station from, Station to) {
        BigDecimal price = BigDecimal.ZERO;
        var lineIterator = new RailLineIterator(train.getLine());

        while (!lineIterator.isChangePoint(from)) {
            lineIterator.getNext().getStartName();
        }

        while (!lineIterator.isChangePoint(to)) {
            price = price.add(lineIterator.getNext().getPrice());
        }

        return price;
    }

}
