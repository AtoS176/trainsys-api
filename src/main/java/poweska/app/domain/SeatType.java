package poweska.app.domain;

import lombok.Getter;

@Getter
public enum SeatType {
    STUDENT(0.40),
    SENIOR(0.60),
    INVALID(0.80),
    NORMAL(0.0);

    double discount;

    SeatType(double discount) {
        this.discount = discount;
    }

}
