package poweska.app.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Set;

@Getter
@Builder
@AllArgsConstructor
@Entity
@Immutable
@Table(name = "station_details")
public class StationDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @OneToMany(mappedBy = "start")
    private Set<LineFragment> asStart;

    @OneToMany(mappedBy = "end")
    private Set<LineFragment> asEnd;

    @Column(name = "platform")
    private int platform;

    public StationDetails() {
    }

    public boolean equalsByStation(Station station) {
        return this.station.equals(station);
    }

    public String getName() {
        return station.getName();
    }

}
