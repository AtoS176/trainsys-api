package poweska.app.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@Builder
@Entity
@Immutable
@Table(name = "line_fragment")
public class LineFragment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    private int fragmentNumber;

    @Column(name = "duration")
    private int duration;

    @Column(name = "distance")
    private int distance;

    @Column(name = "price", precision = 2)
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "start_id")
    private StationDetails start;

    @ManyToOne
    @JoinColumn(name = "end_id")
    private StationDetails end;

    @ManyToOne
    @JoinColumn(name = "line_id")
    private RailLine line;

    public LineFragment() {
    }

    public String getStartName() {
        return start.getName();
    }

    public String getEndName() {
        return end.getName();
    }

    public int getStartPlatform() {
        return start.getPlatform();
    }

    public int getEndPlatform() {
        return end.getPlatform();
    }

    public boolean isStartStation(Station station) {
        return start.equalsByStation(station);
    }

    public boolean isEndStation(Station station) {
        return end.equalsByStation(station);
    }

}
