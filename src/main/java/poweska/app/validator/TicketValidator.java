package poweska.app.validator;

import org.springframework.stereotype.Component;
import poweska.app.dto.TicketDTO;
import poweska.app.util.staticMethod.EmptyListChecker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class TicketValidator {
    private final Pattern pattern = Pattern.compile("^.+@.+\\..+$");

    public boolean isOk(TicketDTO ticket) {
        if (EmptyListChecker.isNullOrEmpty(ticket.getSeatsInTrains())) {
            return false;
        }

        String email = ticket.getEmail();
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}

