package poweska.app.validator;

import org.springframework.stereotype.Component;
import poweska.app.dto.ReservationDTO;
import poweska.app.util.staticMethod.EmptyListChecker;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ReservationValidator {
    private final Pattern pattern = Pattern.compile("^.+@.+\\..+$");

    public boolean isOk(ReservationDTO reservation){
        if(EmptyListChecker.isNullOrEmpty(reservation.getSeatTypes())){
            return false;
        }

        if(reservation.getWhen().isBefore(LocalDate.now())){
            return false;
        }

        Matcher matcher = pattern.matcher(reservation.getEmail());
        return matcher.matches();
    }

}
