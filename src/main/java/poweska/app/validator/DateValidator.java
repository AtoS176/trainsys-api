package poweska.app.validator;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Component
public class DateValidator {
    public LocalDateTime isOk(String when) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(when, formatter);

        return !date.isBefore(LocalDate.now()) ?
                LocalDateTime.of(date, LocalTime.of(0, 0)) :
                null;
    }
}
