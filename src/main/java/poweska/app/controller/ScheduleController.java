package poweska.app.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import poweska.app.dto.TrainDTO;
import poweska.app.service.ScheduleService;

import java.util.List;

@RestController
public class ScheduleController {
    private final ScheduleService scheduleService;

    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @CrossOrigin
    @GetMapping(value = "/schedule")
    public List<TrainDTO> getSchedule(@RequestParam(name = "name") String name) {
        return scheduleService.createSchedule(name);
    }
}
