package poweska.app.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import poweska.app.dto.ReservationDTO;
import poweska.app.dto.TicketDTO;
import poweska.app.service.SeatService;
import poweska.app.validator.ReservationValidator;

import java.util.*;

@RestController
public class SeatsController {
    private final ReservationValidator reservationValidator;
    private final SeatService seatService;

    public SeatsController(ReservationValidator reservationValidator, SeatService seatService) {
        this.reservationValidator = reservationValidator;
        this.seatService = seatService;
    }

    @CrossOrigin
    @PostMapping(value = "/seats/prepareTicket", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> prepareTicket(@RequestBody ReservationDTO reservation){
        if(reservationValidator.isOk(reservation)) {
            Optional<TicketDTO> ticket = seatService.findSeatsToReserve(reservation);
            return ticket.isPresent() ?
                    ResponseEntity.ok(ticket.get()) :
                    ResponseEntity.ok("{\"Error\": \"Brak wolnych miejsc, sprawdz inne połączenie.\"}");
        }
        return ResponseEntity.ok("{\"Error\": \"Wprowadzone dane nie są poprawne.\"}");
    }

}
