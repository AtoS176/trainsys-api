package poweska.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import poweska.app.service.GraphService;
import poweska.app.validator.DateValidator;

import java.time.LocalDateTime;

@RestController
public class GraphController {
    private final DateValidator dateValidator;
    private final GraphService graphService;

    public GraphController(DateValidator dateValidator, GraphService graphService) {
        this.dateValidator = dateValidator;
        this.graphService = graphService;
    }

    @CrossOrigin
    @GetMapping(value = "/graph/findConnection")
    public ResponseEntity<?> findConnections(@RequestParam(name = "from") String from,
                                             @RequestParam(name = "to") String to,
                                             @RequestParam(name = "when") String when) {

        LocalDateTime whenTime = dateValidator.isOk(when);
        return whenTime == null ?
                ResponseEntity.ok("Error : wrong date") :
                ResponseEntity.ok(graphService.findConnections(from, to, whenTime));
    }

}
