package poweska.app.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import poweska.app.dto.TicketDTO;
import poweska.app.service.ReservationService;
import poweska.app.validator.TicketValidator;

@RestController
public class ReservationController {
    private final ReservationService reservationService;
    private final TicketValidator ticketValidator;

    public ReservationController(ReservationService reservationService, TicketValidator ticketValidator) {
        this.reservationService = reservationService;
        this.ticketValidator = ticketValidator;
    }

    @CrossOrigin
    @PostMapping(value = "/reservation/buy", consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean reserveTicket(@RequestBody TicketDTO ticketDTO){
        if(ticketValidator.isOk(ticketDTO)) {
            reservationService.reserveTicket(ticketDTO);
            return true;
        }
        return false;
    }

    @CrossOrigin
    @GetMapping(value = "/reservation/cancel")
    public boolean cancelTicket(@RequestParam(name = "ticketId") Long ticketId,
                                @RequestParam(name = "email") String email,
                                @RequestParam(name = "code") String code){

        return reservationService.cancelTicket(email, ticketId, code);
    }

}
