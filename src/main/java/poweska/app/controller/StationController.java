package poweska.app.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import poweska.app.domain.Station;
import poweska.app.repository.readOnly.StationRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class StationController {
    private final StationRepository stationRepository;

    public StationController(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    @CrossOrigin
    @GetMapping(value = "/stations/getAll")
    public List<String> getStations(){
        return stationRepository.findAll()
                .stream()
                .map(Station::getName)
                .sorted(String::compareTo)
                .collect(Collectors.toUnmodifiableList());
    }
}
