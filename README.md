<h1>Train System Backend</h1>

<p>Web application which allows you to find train connections between cities in Poland. Clients have also
the ability to reserve/cancel tickets and check timetable for the specific city. The main algorithm to
find connections is based on DFS.
</p>

<h2>Configuration</h2>

<h5>1. Create relational database ( I use MariaDB ).</h5>

```
mariadb -u my_username -p
CREATE DATABASE my_database
```


<h5>2.Create application.properties file in resources package.</h5>

```
spring.datasource.url=jdbc:mysql://${my_username:localhost}:3306/my_database?serverTimezone=my_time_zone
spring.datasource.username=my_username
spring.datasource.password=my_password

spring.jpa.database-platform=org.hibernate.dialect.MariaDBDialect
spring.jpa.hibernate.ddl-auto=update

spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=my_email_username
spring.mail.password=my_email_username
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true

#After first application run, you should comment two lines below. It's only serve to initialize data.

spring.datasource.initialization-mode=always
spring.datasource.data=classpath:data.sql
```


<h5>3. Run application.</h5>

`gradle bootRun`